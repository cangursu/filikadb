# ORC Stream Server Development Environment Preparation Steps

For more detailed information please refer : filikadb/research/OrcInMem/doc

## Install Minimal CentOS
Download and install  
http://isoredirect.centos.org/centos/7/isos/x86_64/CentOS-7-x86_64-Minimal-1810.iso
(Support sufficient memory & CPU)

Note : For virtual environments, support IP connectivity both with host and internet 
(VirtualBox : https://serverfault.com/questions/225155/virtualbox-how-to-set-up-networking-so-both-host-and-guest-can-access-internet)

## Upgrade the OS
Login as root.
`$ yum upgrade`

## Install fundamental development tools

Login as root.

`$ yum install git`\
`$ yum install readline-devel`\
`$ yum install zlib-devel`\
`$ yum install bison`\
`$ yum install flex`\
`$ yum install vim`\
`$ yum install bzip2`\
`$ yum install wget`\
`$ yum install libuuid-devel`\
\
`$ yum install centos-release-scl`\
`$ yum install devtoolset-7`\
\
`$ yum install epel-release`\
`$ yum install cmake3`


## Enable DevToolSet-7
`$ scl enable devtoolset-7 bash`

## Prepare cmake3 
Login as root

`$ cd /usr/bin`\
`$ ln -s cmake3 cmake`

## Configure git

`$ git config --global user.name "Your Name"`\
`$ git config --global user.email you@example.com`

## Stop Firewall

`$ systemctl stop firewalld`\
`$ systemctl disable firewalld`

## Install SFTP & SAMBA & SSH Connectivity
Login as root

`$ yum install vsftpd`
https://www.liquidweb.com/kb/how-to-install-and-configure-vsftpd-on-centos-7

`$ yum install samba samba-client samba-common`
https://www.howtoforge.com/samba-server-installation-and-configuration-on-centos-7

Connect via SSH & FTP from host (windows)


## Add to .bashrc
`$ source scl_source enable devtoolset-7`


## Create a postgres/postgres user/pwd

`$ useradd postgres`\
`$ passwd postgres`\
    -> postgres
`$ usermod -g wheel postgres`   #  (add user to sudors goup)\


## Clone PostgreSQL sources

Login as postgres
git://git.postgresql.org/git/postgresql.git    (branch:)

`$ su postgres`\
`$ cd ~/projects/`\
`$ mkdir postgresgl-11`\
`$ git clone -b REL_11_STABLE git://git.postgresql.org/git/postgresql.git  postgresgl-11`

## Build, install & start PostgreSQL

https://www.postgresql.org/docs/current/static/install-procedure.html

`$ su postgres`\
`$ cd ~/projects/postgresgl-11`\
`$ mkdir build`\
`$ cd build`\
`$ ../configure` [options go here]\
( `$ ../configure --enable-cassert --enable-debug CFLAGS="-ggdb" ` )\
`$ make`\
`$ sudo make install `


## Prepare PostgreSQL Server

add to .bash_profile
`export LD_LIBRARY_PATH=/usr/local/pgsql/lib`\
`export PATH=/usr/local/pgsql/bin:$PATH`

- Prepare DB Data files.
>`$ sudo mkdir ~/pgdata`\
`$ sudo chown postgres ~/pgdata`\
`$ initdb -D ~/pgdata --pwprompt `

-  Configure DB
> `edit ~/pgdata/postgresql.conf`
listen_addresses = '192.168.56.101'     # (--> Server IP)
port = 5432
`edit ~/pgdata/pg_hba.conf`
host    all      all     192.168.56.0/24     trust

- Start PostgreSQL Server
>`$ pg_ctl -D ~/pgdata -l ~/pgdata/log start`

- Reload PostgreSQL serverReload PostgreSQL server
>`$ pg_ctl -D ~/pgdata -l ~/pgdata/log restart`\
or
`$ pg_ctl -D ~/pgdata -l ~/pgdata/log start`\
`$ pg_ctl -D ~/pgdata -l ~/pgdata/log stop`

## Build Scirpts
clone "Stream Server" source codes 
Login as Postgres
`$ git clone https://medyasoft_prodigy@bitbucket.org/medyasoft/filikadb.git`

- Build scrips which do not require any PostgreSQLpreparation
> clean_tools.sh (Deletes all build outputs)\
> build_tools.sh (Executes the build processes)\
> check_tools.sh (Executes Unit Test routines)\
> rebuild_tools.sh (Cleans, build and executes Unit Tests)\

- Build scrios which require PostgreSQL installation
> build_tools.sh (Deletes all build outputs)\
> check_tools.sh (Executes the build processes)\
> clean_tools.sh (Executes Unit Test routines)\
> rebuild_tools.sh (Cleans, build and executes Unit Tests)\

