#!/bin/sh

echo ............................................................................
echo Test All 
echo ............................................................................

sh exec.sh "ssrv_check.sh"
res=$?

if [ "$res" != "0" ]; then
    echo ERROR ....
    exit $res
fi

sh exec.sh "ssrv_check_pgext.sh"
res=$?

exit $res
