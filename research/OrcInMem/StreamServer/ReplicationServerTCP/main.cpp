
#include "ReplicationServer.h"
#include "ReplicationServerClient.h"

#include "Console.h"

#include <thread>

#define KEY_EVENT_ID_HELP 0
#define KEY_EVENT_ID_QUIT 1
#define KEY_EVENT_ID_LF   2


class MyConsole : public Console
{
public:
    MyConsole(ReplicationServer<SocketTCP, ReplicationServerClient> &server)
        :_server (server)
    {
    }
    virtual void EventFired(const Console::KeyHandler &kh)
    {
        switch(kh._id)
        {
            case KEY_EVENT_ID_HELP  :   DisplayHelp();  break;
            case KEY_EVENT_ID_QUIT  :   _server.LoopListenStop(); LoopStop();  break;
            case KEY_EVENT_ID_LF    :   std::cout   << std::endl << std::endl << std::endl << std::endl << std::endl << std::endl; break;
        }
    }

private :
    ReplicationServer<SocketTCP, ReplicationServerClient> &_server;
};




// *************************************************************************
// ***                                                                   ***
// ***                   APP                                             ***
// ***                                                                   ***
// ***                                                                   ***
// *************************************************************************

int main(int argc, char** argv)
{
    LogLineGlbSocketName(nullptr);

    ReplicationServer<SocketTCP, ReplicationServerClient> srv("ReplicationServer");

    MyConsole con(srv);
    con.KeyHandlerAdd({'h', 'H'}            , KEY_EVENT_ID_HELP, "h"       , "Help");
    con.KeyHandlerAdd({'q', 'Q', 'x', 'X'}  , KEY_EVENT_ID_QUIT, "q"       , "Quit");
    con.KeyHandlerAdd({'\n', '\r'}          , KEY_EVENT_ID_LF  , "Enter"   , "Line feed");

    // Prepare Server
    const char *port = (argc > 1) ? argv[1] : "5001";
    srv.Port(std::stoi(port));

    con.DisplayMsg("\nReplication Server Test Application");
    con.DisplayMsg("Server Port : ") << srv.Port() << std::endl << std::endl;
    con.DisplayHelp();

    if (SocketResult::SR_SUCCESS != srv.Init())
    {
        con.DisplayErrMsg("Unable to initialize Replication Server\n");
        return -1;
    }


    std::thread th( [&srv](){srv.LoopListen(); } );

    con.DisplayMsg("Packet Server Listen Loop Entered\n");
    con.LoopStart();
    if (th.joinable()) th.join();
    con.DisplayMsg("Packet Server Listen Loop Quitted\n");

    return 0;
}

