/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javarepclient;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.CRC32;
import java.util.zip.Checksum;
        


/**
 *
 * @author can.gursu
 */
public class JavaRepClient 
{
    private Socket          _sck;
    private BufferedReader  _in;

    
    public void startConnection(String ip, int port) 
    {
        try 
        {
            _sck = new Socket(ip, port);
//            _out = _sck.getOutputStream();
            _in  = new BufferedReader(new InputStreamReader(_sck.getInputStream()));
            
        }
        catch (IOException e) 
        {
            System.out.println("Error when initializing connection : " + e);
        }
    }
    


    public void stopConnection() 
    {
        try 
        {
            if (_in  != null) _in.close();
            if (_sck != null) _sck.close();
        }
        catch (IOException e) 
        {
            System.out.println("error when closing : " + e);
        }
    }

    public boolean SendByte(List<Byte> packet) 
    {
        byte[] byArr = new byte[packet.size()];
        for (int i = 0; i < packet.size(); ++i) byArr[i] = packet.get(i);
        return SendByte(byArr);
    }

    public boolean SendByte(byte [] msg) 
    {
        boolean res = false;
        try 
        {
            _sck.getOutputStream().write(msg);
            _sck.getOutputStream().flush();
            res = true;
        }
        catch (Exception e) 
        {
            System.out.println("error when sending : " + e);
        }
        
        return res;
    }


    public void sendPacket(byte [] payload) 
    {
        try 
        {
            List<Byte> packet = new ArrayList<Byte>();
            
            // Magic ID part
            byte mid[] = "MYSF".getBytes();
            for (byte by : mid) packet.add(by);
            
            // Length part
            int length = payload.length;
            for (int i = 0; i < 4; i++) packet.add((byte)(length >>> (i * 8)));

            // Payload part
            for (byte by: payload) packet.add(by);
            
            // CRC part
            Checksum checksum = new CRC32();
            checksum.update(payload, 0, payload.length);
            long checksumValue = checksum.getValue();
            for (int i = 0; i < 4; i++) packet.add((byte)(checksumValue >>> (i * 8)));
            
            SendByte(packet);
        }
        catch (Exception e) 
        {
            System.out.println("error when sending : " + e);
        }
    }
}
