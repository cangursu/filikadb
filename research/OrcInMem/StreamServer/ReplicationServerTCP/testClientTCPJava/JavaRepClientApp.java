/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javarepclient;

import static java.time.Instant.now;
import org.json.simple.*;
import java.util.Base64;
import java.util.UUID;

/**
 *
 * @author can.gursu
 */
public class JavaRepClientApp 
{
    public static void main(String[] args) 
    {
        try
        {
            System.out.println("ReplicationAgent InitialTemplate");
            JavaRepClient client = new JavaRepClient ();
            client.startConnection("192.168.56.104" , 5001);
           
                
            JSONArray arr = new JSONArray(); 

            JSONObject cmdItem = new JSONObject();
            cmdItem.put("Sender",    "JTestClient");
            cmdItem.put("Timestamp", String.valueOf(now().getEpochSecond()));
            cmdItem.put("Source",    "TableXXX");
            cmdItem.put("CmdID",     "WRITE");
            cmdItem.put("RefID",     UUID.randomUUID().toString());
            cmdItem.put("Status",    "REQUEST");
            cmdItem.put("Message",   "Success");
        
            JSONObject buffItem = new JSONObject();
        
            byte [] contentB64 = Base64.getEncoder().encode("XYZ".getBytes("UTF-8"));

            buffItem.put("Enc"    , "Base64");
            buffItem.put("Length" , String.valueOf(contentB64.length));
            buffItem.put("Content", new String(contentB64, "UTF-8"));

            cmdItem.put("Buffer", buffItem);

            arr.add(cmdItem);
            
            JSONObject doc = new JSONObject();
            doc.put("MYSFRIF",     arr);


            /*
            {
                "MYSFRIF": 
                [
                    {
                        "Sender": "TEST-MyClient",
                        "Timestamp": "1548081848",
                        "Source": "TableXXX",
                        "CmdID": "WRITE",
                        "RefID": "f7a154f7-321a-4c57-a628-eaa3dbd58fc7",
                        "Status": "REQUEST",
                        "Buffer": {
                            "Lenght": "16",
                            "Enc": "Base64",
                            "Content": "MTIzNDU2Nzg5MA=="
                        }
                    }
                ]
            }
            */

            System.out.println("Sending : \n" + doc);

            client.sendPacket( doc.toString().getBytes() );
            client.stopConnection();

            System.out.println(doc);
            System.out.println("ReplicationAgent InitialTemplate DONE");
        }
        catch(Exception e)
        {
        }
    }    
}
