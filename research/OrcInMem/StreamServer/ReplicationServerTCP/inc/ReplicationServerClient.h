/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   ReplicationServerClient.h
 * Author: can.gursu
 *
 * Created on 17 October 2018, 11:07
 */

#ifndef __ORC_REMOTE_SERVER_CLIENT_H__
#define __ORC_REMOTE_SERVER_CLIENT_H__

#include "SocketClientPacket.h"
#include "SocketClientPacketSyncResponse.h"
#include "Console.h"
#include "SocketTCP.h"
//#include "MemStreamMap.h"
#include "MemStreamStore.h"
#include "MemStreamPacket.h"

#include <ctime>


class ReplicationServerClient : public SocketClientPacket<SocketTCP>
{
    using  msize_t = StreamPacket::msize_t;
public:
    ReplicationServerClient(const char *name = "ReplicationServerClient")
            : SocketClientPacket<SocketTCP>(name)
    {
    }

    ReplicationServerClient(int fd, const char *name)
        : SocketClientPacket<SocketTCP>(fd, name)
    {

    }

    virtual void OnErrorClient (SocketResult err)
    {
        std::cerr << "Error : ReplicationServerClient::OnErrorClient - " << this->Name() << " : " << SocketResultText(err) << std::endl;
    }

    void            DisplayPacket       (StreamPacket &packet, const char *msg = nullptr);

    virtual void    OnRecvPacket        (StreamPacket &&packet)
    {
//        std::cout << std::endl;
//        std::cout << packet.Dump("Raw Packet");
//        std::cout << std::endl;
        std::cout << packet.DumpPayload("Payload");
        //DisplayPacket(packet);

        std::list <MemStreamPacket::Cmd> cmdList;

        if ( 0 < MemStreamPacket::DecodePacket(packet, cmdList))
        {
            std::cout << "Command Count " << cmdList.size() << std::endl;
            for (const auto &cmd : cmdList)
            {
                std::cout << cmd.Dump()                << std::endl;

                MemStreamPacket pckResp;
                pckResp.CreatePacketResponse("ReplicationServer", cmd, MemStreamPacket::CmdStatus::CST_SUCCESS, "Suceeded");
                SendPacket(std::move(pckResp));
            }
        }
    }

    void Silent(bool set) { _isSilent = set; }
    bool Silent()         { return _isSilent; }

private:
    bool           _isSilent = false;
};





inline void ReplicationServerClient::DisplayPacket(StreamPacket &packet, const char *msg /* = nullptr */)
{
    const msize_t        buffLen = 512;
    StreamPacket::byte_t buff [buffLen];

    if (msg)
        std::cout << "Packet : " << msg << std::endl;

    msize_t pyLenPart = 0;
    msize_t pyLen = packet.PayloadLen();
    for (msize_t offset = 0; offset < pyLen; offset += buffLen)
    {
        if ( (pyLenPart = packet.PayloadPart(buff, buffLen, offset)) > 0)
            std::cout << std::string((char*)buff, pyLenPart);
    }
    std::cout << std::endl;
}

#endif /* __ORC_REMOTE_SERVER_CLIENT_H__ */

