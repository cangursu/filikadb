/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "main.h"
#include "SocketTCP.h"

#include <gtest/gtest.h>
#include <mutex>
#include <condition_variable>
#include <list>


TEST(MemStreamServerSyncTCP, Individual)
{
    PacketEchoServer<SocketTCP, SocketTCP>  server;

    STestClientSync<SocketTCP> client;

    server.Address("127.0.0.1", 5001);
    client.Address("127.0.0.1", 5001);

    const char *data[] =
    {
        "XXX",
        "TestDataYYY",
        "TestData_12345",
        "TestData_67890",
        "TestData_0987654321",
        "TestData_abcdefghijklmnopqrstuvwxyz"
    };

    ClientServerFrame(server, client,   [&data] (auto &client)
                                        {
                                            for (const auto &item : data)
                                            {
                                                StreamPacket  packetReturn;
                                                StreamPacket  packet (item, strlen(item));

                                                SocketResult res = client.SendPacket(packet, packetReturn);
                                                ASSERT_TRUE(SocketResult::SR_SUCCESS == res);
                                                if (SocketResult::SR_SUCCESS == res)
                                                {
                                                    ASSERT_TRUE(packetReturn.Check());
                                                    ASSERT_TRUE(packetReturn == packet);
                                                }
                                            }
                                        }
    );


    server.Release();
}



TEST(MemStreamServerSyncTCP, IndividualMClient)
{
    PacketEchoServer<SocketTCP, SocketTCP>  server;
    server.Address("127.0.0.1", 5001);

    int countClient = 250;
    std::deque<STestClientSync<SocketTCP>> clientList;

    for(int i = 0; i < countClient; ++i)
    {
        STestClientSync<SocketTCP> &c = clientList.emplace_back((std::string("STestClient") + std::to_string(i)).c_str());
        c.Address("127.0.0.1", 5001);
    }

    const StreamPacket data[] =
    {
        "XXX",
        "TestDataYYY",
        "TestData_12345",
        "TestData_67890",
        "TestData_0987654321",
        "TestData_abcdefghijklmnopqrstuvwxyz"
    };
    const int countData = sizeof(data)/sizeof(*data);

    ClientServerFrameMClient(
        server, clientList, [&](auto &clientList)
                            {
                                for (auto &client : clientList)
                                {
                                    for(auto &item : data)
                                    {
                                        StreamPacket recievedPacket;
                                        EXPECT_EQ(SocketResult::SR_SUCCESS, client.SendPacket(item, recievedPacket));
                                        EXPECT_EQ(item, recievedPacket);
                                    }
                                }
                            }
    );
    server.Release();
}



static std::mutex              g_mtx;
static std::condition_variable g_cv;
static bool                    g_ready = false;

auto thFunctor = [](std::array<StreamPacket, 6> &testPackets) -> void
{
    SocketResult res;

    std::stringstream ss;
    ss << "STestClient" << std::this_thread::get_id();
    STestClientSync<SocketTCP> client(ss.str().c_str());
    client.Address("127.0.0.1", 5001);

    EXPECT_EQ(SocketResult::SR_SUCCESS, res = client.Init());
    if (SocketResult::SR_SUCCESS != res) return;

    EXPECT_EQ(SocketResult::SR_SUCCESS, res = client.ConnectServer());
    if (SocketResult::SR_SUCCESS != res) return;

    msleep(1);
    std::thread thRead = std::thread( [&client](){ client.LoopStart(); });

    std::unique_lock<std::mutex> lk(g_mtx);
    g_cv.wait(lk, []{return g_ready;});
    lk.unlock();

    for(const auto &pck : testPackets)
    {
        StreamPacket recievedPacket;
        EXPECT_EQ(SocketResult::SR_SUCCESS, client.SendPacket(pck, recievedPacket));
        EXPECT_EQ(pck, recievedPacket);
    }

    client.LoopStop();
    if (thRead.joinable()) thRead.join();
};



TEST(MemStreamServerSyncTCP, IndividualConcurrent)
{
    PacketEchoServer<SocketTCP, SocketTCP>  server;
    server.Address("127.0.0.1", 5001);
    ASSERT_EQ(SocketResult::SR_SUCCESS, server.Init());
    std::thread thServer(   [&server](){server.LoopListen();}   );

    std::array<StreamPacket, 6> testPackets
    {
        "TestDataXXX",
        "TestDataYYY",
        "TestData_12345",
        "TestData_67890",
        "TestData_0987654321",
        "TestData_abcdefghijklmnopqrstuvwxyz"
    };

    int countClient = 100;
    std::thread thClients[countClient];

    for(int i = 0; i < countClient; ++i)
        thClients[i] = std::thread(thFunctor, std::ref(testPackets));
    msleep(500);

    {
        std::unique_lock<std::mutex> lk(g_mtx);
        g_ready = true;
        g_cv.notify_all();
        lk.unlock();
    }

    for (auto &thClient : thClients)
        if(thClient.joinable()) thClient.join();

    server.LoopListenStop();
    if (thServer.joinable()) thServer.join();

    server.Release();
}




