/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   main.cpp
 * Author: can.gursu
 *
 * Created on 16 July 2018
 */


#include "SocketClientPacket.h"
#include "SocketClientPacketSyncResponse.h"
#include "SocketTCP.h"
#include "GeneralUtils.h"
#include "StreamPacket.h"

#include <iostream>
#include <cstring>
#include <unistd.h>
#include <thread>



template<typename TSocket>
class SClientSync : public SocketClientPacketSyncResponse<TSocket>
{
    public:
        SClientSync(const char *name): SocketClientPacketSyncResponse<TSocket>(name) {}
};

template<typename TSocket>
class SClientASync : public SocketClientPacket<TSocket/*SocketTCP*/>
{
    public:
        SClientASync(const char *name): SocketClientPacket<TSocket/*SocketTCP*/>(name) {}
//        virtual void OnRecv        (MemStream<std::uint8_t> &&);
        virtual void OnErrorClient (SocketResult);
        virtual void OnRecvPacket  (StreamPacket &&packet);
};

template<typename TSocket>
inline void SClientASync<TSocket>::OnErrorClient (SocketResult err)
{
    std::cout << "ERROR : SClient::OnErrorClient - " << SocketResultText(err) << std::endl;
}

template<typename TSocket>
inline void SClientASync<TSocket>::OnRecvPacket  (StreamPacket &&packet)
{
    std::cout << packet.DumpPayload("SClient<TClient>::OnRecvPacket");
    /*
    StreamPacket::msize_t         pyLen = packet.PayloadLen();
    const StreamPacket::msize_t   buffLen = 128;
    StreamPacket::byte_t          buff [buffLen];

    for (StreamPacket::msize_t i = 0; i < pyLen; i += buffLen)
    {
        if (packet.PayloadPart(buff, buffLen, i) > 0)
        {
            //LOG_LINE_GLOBAL("ClientEcho", "Packet:", std::string((char*)buff, pyLen));
            //std::cout << "Packet Reveived : " << std::string((char*)buff, pyLen) << std::endl;
        }
    }
    */
}










bool send(SocketTCP &sock, const void *data, std::uint32_t len)
{
    ssize_t res = (len > 0) ? sock.Write(data, len) : 0;
    return ((res > 0) && (res == (ssize_t)len));
}


bool send(SocketTCP &sock, const StreamPacket &pack)
{
    const StreamPacket::byte_t *b;
    std::uint32_t l = pack.Buffer(&b);

    ssize_t res = (l > 0) ? sock.Write(b, l) : 0;
    return ((res > 0) && (res == (ssize_t)l));
}


bool send_StreamPacket(SocketTCP &sock, const char *data)
{
    std::cout << "Sending : " << data << std::endl;
    return send(sock, std::move(StreamPacket(data, strlen(data))));
}


void SendMultipleIndividual(SocketTCP &sock)
{
    char  buff[32] = "MulIndividual-";
    for (int i = 0; i < 100; ++i)
    {
        strcpy(buff+11, std::to_string(i).c_str());
        send(sock, std::move(StreamPacket(buff, std::strlen(buff))));
    }
}


void SendBulkIndividual(SocketTCP &sock)
{
    std::string           prefix = "BulkIndividual-";
    StreamPacket::byte_t  buff2[4096];
    StreamPacket::byte_t *pBuff = nullptr;
    int  len    = 0;
    int  lenPck = 0;

    for (int i = 0; i < 100; ++i)
    {
        std::string data = prefix +  std::to_string(i);
        StreamPacket packet(data.c_str(), data.size());

        lenPck = packet.Buffer(&pBuff);
        std::memcpy(buff2 + len, pBuff, lenPck);
        len += lenPck;
    }

    std::cout << "sending  len = " << len << std::endl;
    send(sock, buff2, len);
}


void SendDirt(SocketTCP &sock)
{
    std::string           prefix = std::string("Dirt-")+std::to_string(getpid());
    StreamPacket::byte_t  buff[4096];
    StreamPacket::byte_t *pBuff  = nullptr;
    int                   len    = 0;
    int                   lenPck = 0;

    std::string data = prefix + "Individual";
    StreamPacket packet(data.c_str(), data.size());

    lenPck = packet.Buffer(&pBuff);

    std::memcpy(buff + len, "XXX", 3);
    len+=3;

    std::memcpy(buff + len, pBuff, lenPck);
    len += lenPck;

    std::memcpy(buff + len, "YYY", 3);
    len+=3;

    std::cout << "sending  len = " << len << std::endl;
    send(sock, buff, len);
}


void SendDirtBulk(SocketTCP &sock)
{
    std::string           prefix = "Dirt-";
    StreamPacket::byte_t  buff[4096];
    StreamPacket::byte_t *pBuff = nullptr;
    int  len  = 0;
    int  lenPck = 0;
    for (int i = 0; i < 40; ++i)
    {
        std::string data = prefix +  std::to_string(i);
        StreamPacket packet(data.c_str(), data.size());

        lenPck = packet.Buffer(&pBuff);

        std::memcpy(buff + len, "XXX", 3);
        len+=3;

        std::memcpy(buff + len, pBuff, lenPck);
        len += lenPck;

        std::memcpy(buff + len, "YYY", 3);
        len+=3;
    }

    std::cout << "sending  len = " << len << std::endl;
    send(sock, buff, len);
}


void SendCorrupt(SocketTCP &sock)
{
    std::string           prefix = "Corrupt-Individual";
    StreamPacket::byte_t  buff2[4096];
    StreamPacket::byte_t *pBuff  = nullptr;
    int                   len    = 0;
    int                   lenPck = 0;

    std::string data = prefix;
    StreamPacket packet(data.c_str(), data.size());

    lenPck = packet.Buffer(&pBuff);
    pBuff[13] = 'X';
    std::memcpy(buff2 + len, pBuff, lenPck);
    len += lenPck;

    std::cout << "sending  len = " << len << std::endl;
    send(sock, buff2, len);
}


void SendCorruptBulk(SocketTCP &sock)
{
    std::string           prefix = "Corrupt-";
    StreamPacket::byte_t  buff2[4096];
    StreamPacket::byte_t *pBuff = nullptr;
    int  len    = 0;
    int  lenPck = 0;

    for (int i = 0; i < 1; ++i)
    {
        std::string data = prefix +  std::to_string(i);
        StreamPacket packet(data.c_str(), data.size());

        lenPck = packet.Buffer(&pBuff);
        //pBuff[16] = 'X';
        pBuff[13] = 'X';
        std::memcpy(buff2 + len, pBuff, lenPck);
        len += lenPck;
    }

    std::cout << "sending  len = " << len << std::endl;
    send(sock, buff2, len);
}




int main(int , char** )
{
    const char   *slog  = "/home/postgres/.sock_domain_log";
    //const char   *sname = "127.0.0.1";
    const char   *sname = "192.168.56.104";
    std::uint16_t sport = 5001;

    std::cout << "StreamServerClient v0.0.0.2\n";
    std::cout << "Using Log Server : " << slog << std::endl;
    std::cout << "Using Server     : " << sname << ":" << sport << std::endl;


    LogLineGlbSocketName(slog);
    LOG_LINE_GLOBAL("ClientEcho", "ver 0.0.0.0");


    SClientASync<SocketTCP> sockASync("SenderASync");
    sockASync.Address(sname, sport);

    if (SocketResult::SR_SUCCESS != sockASync.Init())
    {
        std::cerr << "ERROR : Unable to init SocketTCP \n";
        return -1;
    }
    if (SocketResult::SR_SUCCESS != sockASync.ConnectServer())
    {
        std::cerr << "ERROR : Unable to Connect SocketTCP \n";
        return -2;
    }
    std::thread thASync ( [&sockASync] () { sockASync.LoopStart();} );
    //sleep(1);



    SClientSync<SocketTCP> sockSync("SenderSync");
    sockSync.Address(sname, sport);

    if (SocketResult::SR_SUCCESS != sockSync.Init())
    {
        std::cerr << "ERROR : Unable to init SocketTCP \n";
        return -1;
    }
    if (SocketResult::SR_SUCCESS != sockSync.ConnectServer())
    {
        std::cerr << "ERROR : Unable to Connect SocketTCP \n";
        return -2;
    }
    std::thread thSync ( [&sockSync] () { sockSync.LoopStart();} );
    //sleep(1);





    {
        //Invidiual ASync

        char *arr[] = { "xxx", "TestData_12345", "TestData_67890",
                        "TestData_abcde", "TestData_fghiy"  };
        for (auto data : arr)
        {
            std::cout << "Sending : " << data << std::endl;
            sockASync.SendPacket( StreamPacket(data, strlen(data)));
        }

        /*
        send_StreamPacket(sockASync, "xxx");
        send_StreamPacket(sockASync, "TestData_12345");
        send_StreamPacket(sockASync, "TestData_67890");
        sleep(5);
        send_StreamPacket(sockASync, "TestData_abcde");
        send_StreamPacket(sockASync, "TestData_fghiy");
        */
    }


    {
        //Invidiual Sync

        char *arr[] =
        {
            "yyy",
            "TestData_12345",
            "TestData_67890",
            "TestData_abcde",
            "TestData_fghiy",
        };

        for (auto data : arr)
        {
            StreamPacket returnedPacket;
            std::cout << "Sending : " << data << std::endl;
            SocketResult res = sockSync.SendPacket(StreamPacket(data, strlen(data)), returnedPacket);
            std::cout << "res = " << SocketResultText(res) << std::endl << returnedPacket.DumpPayload("returnedPacket") << std::endl;
        }
    }


/*
    {
        //Combined Individual
        StreamPacket pck1("TestDataCombinedPacket_1_1x345", 30);
        StreamPacket pck2("TestDataCombinedPacket_2_6x890", 30);

        StreamPacket::byte_t *bufferPrt1;
        StreamPacket::byte_t *bufferPrt2;

        int  lenBuffer1 = pck1.Buffer(&bufferPrt1);
        int  lenBuffer2 = pck2.Buffer(&bufferPrt2);

        StreamPacket::byte_t data[lenBuffer1 + lenBuffer2];
        std::memcpy(data,            bufferPrt1, lenBuffer1);
        std::memcpy(data+lenBuffer1, bufferPrt2, lenBuffer2);

        send(sockASync, data, lenBuffer1 + lenBuffer2);
    }
*/

/*
    {
        //Fragmanted
        StreamPacket pck("TestDataFragmanted67890", 23);

        StreamPacket::byte_t *bufferPrt;
        int  lenBuffer = pck.Buffer(&bufferPrt);

        int  lenPart1 = lenBuffer/2;
        StreamPacket::byte_t part1[lenPart1];
        std::memcpy(part1, bufferPrt, lenPart1);
        send(sockASync, part1, lenPart1);
        sleep(2);

        int     lenPart2 = lenBuffer - lenPart1;
        StreamPacket::byte_t  part2[lenPart2];
        std::memcpy(part2, bufferPrt+lenPart1, lenPart2);
        send(sockASync, part2, lenPart2);
    }
*/

/*
    {
        //Dirt
        StreamPacket pck("TestDataDirt:abcdefg", 20);

        StreamPacket::byte_t *bufferPrt;
        int lenBuffer = pck.Buffer(&bufferPrt);

        StreamPacket::byte_t data[lenBuffer + 10];
        data[0] = 'A';
        data[1] = 'B';
        data[2] = 'C';
        data[3] = 'D';
        std::memcpy(data+4, bufferPrt, lenBuffer);
        data[4 + lenBuffer + 1] = 'E';
        data[4 + lenBuffer + 2] = 'F';
        data[4 + lenBuffer + 3] = 'G';
        data[4 + lenBuffer + 4] = 'H';

        send(sockASync, data, lenBuffer+8);
    }
*/

/*
    {
        //Corrupt
        StreamPacket pck("hijklmnopqrstuvwxyz", 19);

        StreamPacket::byte_t *bufferPrt;
        int lenBuffer = pck.Buffer(&bufferPrt);
        bufferPrt[16] = '.';

        std::cout << "Sending : Corrupted Packet\n";
        send(sockASync, pck);

        send_StreamPacket(sockASync, "yyy");
    }
*/

/*
    for(int i = 0; i < 50; ++i)
    {
        SendMultipleIndividual(sockASync);
        SendBulkIndividual(sockASync);

        SendBulkDirt(sockASync);
        SendBulkCorrupt(sockASync);

        send(sockASync, std::move(StreamPacket("12345", 5)));
    }
*/

/*
    {
        send_StreamPacket(sockASync, (std::string("aaa.") + std::to_string(getpid())).c_str() );
        SendDirt(sockASync);
        send_StreamPacket(sockASync, (std::string("bbb.") + std::to_string(getpid())).c_str() );
        SendCorrupt(sockASync);
        send_StreamPacket(sockASync, (std::string("ccc.") + std::to_string(getpid())).c_str() );
    }
*/


    std::cout << "Sleeping before exit\n";
    sleep(10);
    //std::cout << "<\n";

    sockASync.LoopStop();
    sockSync.LoopStop();
    if (thASync.joinable()) thASync.join();
    if (thSync.joinable())  thSync.join();

    return 0;
}

