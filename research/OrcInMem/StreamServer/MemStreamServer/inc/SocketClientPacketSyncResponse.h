/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   SocketClientPacket.h
 * Author: can.gursu
 *
 * Created on 19 November 2018, 17:47
 */


#ifndef __SOCKET_CLIENT_PACKET_SYNC_RESPONSE_H__
#define __SOCKET_CLIENT_PACKET_SYNC_RESPONSE_H__


#include "SocketClientPacket.h"
#include "MemStreamPacket.h"
#include <chrono>



template <typename TSocket>
class SocketClientPacketSyncResponse : public SocketClientPacket<TSocket>
{
public:
    SocketClientPacketSyncResponse(const char *name = "SocketClientPacketSyncResponse")
        : SocketClientPacket<TSocket>(name)
    {
    }

    SocketClientPacketSyncResponse(int fd, const char *name)
        : SocketClientPacket<TSocket>(fd, name)
    {
    }
    virtual ~SocketClientPacketSyncResponse()
    {
    }


    SocketResult SendPacketWrite(     const MemStream<StreamPacket::byte_t> &buff
                                    , StreamPacket &packetRecv
                                    , const char *sender
                                    , const char *source)
    {
        SocketResult res = SocketResult::SR_EMPTY;
        enum class IS_DONE {no, error, yes } isDone(IS_DONE::no);

        std::uint32_t lengthBuff = buff.Len();

        const int lengthfragment = _8K;
        StreamPacket::byte_t tempBuff[lengthfragment];

        for (std::uint32_t offset = 0; (isDone == IS_DONE::no) && offset < lengthBuff; offset += lengthfragment)
        {
            std::uint32_t  lengthSend = (offset + lengthfragment) < lengthBuff ? lengthfragment : lengthBuff - offset;

            buff.Read(tempBuff, lengthSend, offset);

            MemStreamPacket packetSend;
            packetSend.CreatePacketWrite(sender, source, lengthSend, /*buff + offset*/tempBuff);
//            std::cout << "packetSend : \n" << packetSend.DumpPayload() << std::endl;

            if (SocketResult::SR_SUCCESS != (res = SendPacket(packetSend, packetRecv)))
            {
                LOG_LINE_GLOBAL("remote", "ERROR: Unable to send packet. result:", SocketResultText(res));
  //              std::cout << "ERROR : SendPacket failed.\n";
                isDone = IS_DONE::error;
            }
            else
            {
//                std::cout << "packetRecv : \n" << packetRecv.DumpPayload() << std::endl;
            }
        }

        return res;
    }


    SocketResult SendPacketRead(    MemStream<StreamPacket::byte_t> &buff
                                  , std::uint32_t lengthBuff
                                  , std::size_t offsetBuff
                                  , StreamPacket &packetRecv
                                  , const char *sender
                                  , const char *source)
    {
        SocketResult res = SocketResult::SR_EMPTY;
        const int lengthfragment = _8K;

        for (std::uint32_t offset = 0; offset < lengthBuff; offset += lengthfragment)
        {
            std::uint32_t  lengthSend = (offset + lengthfragment) < lengthBuff ? lengthfragment : lengthBuff - offset;
/*
            std::cout << "SendPacketRead  "
                      << ": lengthBuff:" << lengthBuff << ", offsetBuff:" << offsetBuff
                      << ", lengthSend:" << lengthSend << ", offset:"     << offset
                      << std::endl;
*/
            MemStreamPacket packetSend;
            packetSend.CreatePacketRead(sender, source, lengthSend, offsetBuff + offset);
//            std::cout << "packetSend : \n" << packetSend.DumpPayload() << std::endl;

            if (SocketResult::SR_SUCCESS != (res = SendPacket(packetSend, packetRecv)))
            {
                LOG_LINE_GLOBAL("remote", "ERROR: Unable to send packet. result:", SocketResultText(res));
//              std::cout << "ERROR : SendPacket failed.\n";
                break;
            }
            else
            {
//                std::cout << "packetRecv : \n" << packetRecv.DumpPayload() << std::endl;
                std::list<MemStreamPacket::Cmd> listCmd;
                MemStreamPacket::DecodePacket(packetRecv, listCmd);
                for (const auto cmd : listCmd)
                {
                    if (cmd._result == "SUCCESS")
                    {
                        buff += cmd.DecodeBuffer();
                    }
                }
            }
        }
        return res;
    }

    SocketResult SendPacket(const StreamPacket &packetSend, StreamPacket &packetRecv)
    {
        _packet.Reset();
        _result = SocketClientPacket<TSocket>::SendPacket(packetSend);

        if (_result == SocketResult::SR_SUCCESS)
        {
            _result = SocketResult::SR_EMPTY;

            std::unique_lock<std::mutex> mlock(_mutex);
            if (std::cv_status::timeout == _cond.wait_for(mlock,  std::chrono::milliseconds(30000)))
            {
                _result = SocketResult::SR_ERROR_SEND;
            }
            else
            {
                _result = SocketResult::SR_SUCCESS;
                packetRecv = std::move(_packet);
            }
        }
        return _result;
    }

private :

    virtual void OnRecvPacket(StreamPacket &&packet) final
    {
        _packet = std::move(packet);
        _result = SocketResult::SR_SUCCESS;

        _cond.notify_one();
        std::lock_guard<std::mutex> guard(_mutex);
    }

    virtual void OnErrorClient (SocketResult res) final
    {
        _cond.notify_one();
        _result = res;
        _packet.Reset();
        std::lock_guard<std::mutex> guard(_mutex);
    }

private :
    std::mutex              _mutex;     //Unable to use as ServerClient
    std::condition_variable _cond;      //Unable to use as ServerClient
    StreamPacket            _packet;
    SocketResult            _result = SocketResult::SR_EMPTY;
};




 #endif // __SOCKET_CLIENT_PACKET_SYNC_RESPONSE_H__

