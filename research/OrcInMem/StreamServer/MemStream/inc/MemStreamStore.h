/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   MemStreamStore.h
 * Author: can.gursu
 *
 * Created on 22 November 2018, 08:09
 */

#ifndef __MEM_STREAM_STORE_H__
#define __MEM_STREAM_STORE_H__

#include "MemStreamMap.h"
#include "StreamPacket.h"
#include "MemStreamPacket.h"

class MemStreamStore
{
public:
    MemStreamStore();
    virtual ~MemStreamStore();

    void    Packet          (StreamPacket &packet, std::vector<MemStreamPacket> &responses, const char *msg = nullptr);

private:
    MemStreamPacket OnCmdWrite  (const MemStreamPacket::Cmd &cmd);
    MemStreamPacket OnCmdRead   (const MemStreamPacket::Cmd &cmd);
    MemStreamPacket OnCmdDelete (const MemStreamPacket::Cmd &cmd);
    MemStreamPacket OnCmdLength (const MemStreamPacket::Cmd &cmd);

    MemStreamMap _map;
};

#endif /* __MEM_STREAM_STORE_H__ */

