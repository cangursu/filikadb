/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   MemStreamStore.cpp
 * Author: can.gursu
 *
 * Created on 22 November 2018, 08:09
 */

#include "MemStreamStore.h"


MemStreamStore::MemStreamStore()
{
}


MemStreamStore::~MemStreamStore()
{
}


void MemStreamStore::Packet (StreamPacket &packet, std::vector<MemStreamPacket> &responses, const char *msg /*= nullptr*/)
{
    //std::cout << packet.Dump("OrcRemoteServerClient::OnRecvPacket") << std::endl;
    //DisplayPacket(packet, "OrcRemoteServerClient::OnRecvPacket");

    std::list <MemStreamPacket::Cmd> cmdList;
    if ( 0 < MemStreamPacket::DecodePacket(packet, cmdList))
    {

        for(const auto &cmd : cmdList)
        {
            if (msg)
                std::cout << cmd.DumpBriefCompact(msg);
            //DisplayCommadBrief(cmd, "OrcRemoteServerClient::OnRecvPacket : ");

            switch (cmd._cmdid)
            {
                case MemStreamPacket::CmdID::CMD_WRITE  : responses.push_back(OnCmdWrite(cmd)  ); break;
                case MemStreamPacket::CmdID::CMD_READ   : responses.push_back(OnCmdRead(cmd)   ); break;
                case MemStreamPacket::CmdID::CMD_DELETE : responses.push_back(OnCmdDelete(cmd) ); break;
                case MemStreamPacket::CmdID::CMD_LENGTH : responses.push_back(OnCmdLength(cmd) ); break;
                default                                 : break;
            }
        }
    }
}


MemStreamPacket MemStreamStore::OnCmdWrite(const MemStreamPacket::Cmd &cmd)
{
    MemStreamPacket pckResponse;
    /*
    if (nullptr == _pmap)
    {
        pckResponse.CreatePacketResponse("OrcRemoteServerClient", cmd, MemStreamPacket::CmdStatus::CST_ERROR, "Server Not Ready");
    }
    else
    */
    {
        size_t buffLen = cmd._buffer._buf.size();
        char buff[buffLen] = "";
        base64_decode (&cmd._buffer._buf[0], cmd._buffer._buf.size(), buff, &buffLen, BASE64_FORCE_AVX2);

        //std::cout << "Write: _source:" << cmd._source << " buff:" << std::string(buff, buffLen) << std::endl;
        /*_pmap->*/_map.Write(cmd._source.c_str(), buff, buffLen);
        pckResponse.CreatePacketResponse("OrcRemoteServerClient", cmd, MemStreamPacket::CmdStatus::CST_SUCCESS, "Write Suceeded");
    }

    return std::move(pckResponse);
}


MemStreamPacket MemStreamStore::OnCmdRead(const MemStreamPacket::Cmd &cmd)
{
    MemStreamPacket pckResponse;
    /*
    if (nullptr == _pmap)
    {
        pckResponse.CreatePacketResponse("OrcRemoteServerClient", cmd, MemStreamPacket::CmdStatus::CST_ERROR, "Server Not Ready");
    }
    else
    */
    {
        char buff[std::strtol(cmd._buffer._length.c_str(), nullptr, 10)] = "";
        std::uint64_t lenrd = /*_pmap->*/_map.Read(cmd._source.c_str(),
                                          (void*)buff, std::strtol(cmd._buffer._length.c_str(), nullptr, 10),
                                          std::strtol(cmd._buffer._offset.c_str(), nullptr, 10));
        //std::cout << "Read: _source:" << cmd._source << " buff:" << std::string(buff, lenrd) << std::endl;

        if ((lenrd < 0) || (lenrd == (std::uint64_t)-1))
        {
            pckResponse.CreatePacketResponse("OrcRemoteServerClient", cmd, MemStreamPacket::CmdStatus::CST_ERROR, "Source Not Found");
        }
        else
        {
            pckResponse.CreatePacketResponse("OrcRemoteServerClient", cmd, MemStreamPacket::CmdStatus::CST_SUCCESS, "Reading Suceeded", lenrd, buff);
        }
    }

    return std::move(pckResponse);
}


MemStreamPacket MemStreamStore::OnCmdDelete(const MemStreamPacket::Cmd &cmd)
{
    MemStreamPacket pckResponse;
    /*
    if (nullptr == _pmap)
    {
        pckResponse.CreatePacketResponse("OrcRemoteServerClient", cmd, MemStreamPacket::CmdStatus::CST_ERROR, "Server Not Ready");
    }
    else
    */
    {
        if (((std::uint64_t)0) == /*_pmap->*/_map.Delete(cmd._source.c_str()))
            pckResponse.CreatePacketResponse("OrcRemoteServerClient", cmd, MemStreamPacket::CmdStatus::CST_ERROR, "Source Not Found");
        else
            pckResponse.CreatePacketResponse("OrcRemoteServerClient", cmd, MemStreamPacket::CmdStatus::CST_SUCCESS, "Deletion Suceeded");
    }

    return std::move(pckResponse);
}


MemStreamPacket MemStreamStore::OnCmdLength(const MemStreamPacket::Cmd &cmd)
{
    MemStreamPacket pckResponse;
    /*
    if (nullptr == _pmap)
    {
        pckResponse.CreatePacketResponse("OrcRemoteServerClient", cmd, MemStreamPacket::CmdStatus::CST_ERROR, "Server Not Ready");
    }
    else
    */
    {
        std::uint64_t len = /*9_pmap->*/_map.Len(cmd._source.c_str());
        if (len == ((std::uint64_t)-1))
            pckResponse.CreatePacketResponse("OrcRemoteServerClient", cmd, MemStreamPacket::CmdStatus::CST_ERROR, "Source Not Found");
        else
            pckResponse.CreatePacketResponse("OrcRemoteServerClient", cmd, MemStreamPacket::CmdStatus::CST_SUCCESS, "Length got successfully", len);
    }

    return std::move(pckResponse);
}
