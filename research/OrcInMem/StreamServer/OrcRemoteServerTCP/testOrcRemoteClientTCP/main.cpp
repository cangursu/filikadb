
#include "SocketClientPacket.h"
#include "SocketClientPacketSyncResponse.h"
#include "SocketTCP.h"
#include "SocketServer.h"
#include "Console.h"
//#include "MemStreamPacketStore.h"
#include "MemStreamPacket.h"


#include <stdio.h>
#include <vector>
#include <thread>
#include <termios.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <istream>
#include <fstream>
#include <unistd.h>

#define DEFAULT_REMOTE_IP   "127.0.0.1"
//#define DEFAULT_REMOTE_IP   "192.168.56.103"
#define DEFAULT_REMOTE_PORT 5001


class MyClient : public SocketClientPacket<SocketTCP>
{
    public:
        MyClient(const char *name) : SocketClientPacket<SocketTCP>(name)
        {
        }

        virtual void OnErrorClient (SocketResult err)
        {
            std::cerr << "SocketClientPacket::OnErrorClient ->" << SocketResultText(err) << std::endl;
        }

        virtual void OnRecvPacket(StreamPacket &&packet)
        {
            //DisplayPacket("Recieved Packet", packet);

            std::list <MemStreamPacket::Cmd> cmds;
            MemStreamPacket stream;
            int count = stream.DecodePacket(packet, cmds);
            for (const auto &cmd : cmds)
            {
                std::cout << cmd.Dump() << std::endl;

            }

        }

        SocketResult SendPacketWrite(const char *source, const char *content)
        {
            return SendPacketWrite(source, content, std::strlen(content));
        }

        SocketResult SendPacketWrite(const char *source, const void *content, size_t length)
        {
            SocketResult res = SocketResult::SR_EMPTY;
            MemStreamPacket packet;
            packet.CreatePacketWrite("TEST-MyClient", source, length, content);

            if (SocketResult::SR_SUCCESS != (res = this->SendPacket(packet)))
            {
                LOG_LINE_GLOBAL("remote", "ERROR: Unable to send packet. errno:", errno);
                std::cout << "ERROR : SendPacket failed.\n";
            }
            else
            {
                //DisplayPacket("Sended Packet", packet);
            }

            return res;
        }
/*
        SocketResult SendPacketReadSync(const char *source, size_t length, size_t offset)
        {
            SocketResult res = SocketResult::SR_EMPTY;
            MemStreamPacket packet;
            std::string refid = packet.CreatePacketRead("TEST-MyClient", source, length, offset);

            if (SocketResult::SR_SUCCESS != (res = this->SendPacket(packet)))
            {
                LOG_LINE_GLOBAL("remote", "ERROR: Unable to send packet. errno:", errno);
                std::cout << "ERROR : SendPacket failed.\n";
            }
            else
            {
                //DisplayPacket("Sended Packet", packet);
            }

            return res;
        }
*/
        SocketResult SendPacketRead(const char *source, size_t length, size_t offset)
        {
            SocketResult res = SocketResult::SR_EMPTY;
            MemStreamPacket packet;
            packet.CreatePacketRead("TEST-MyClient", source, length, offset);

            if (SocketResult::SR_SUCCESS != (res = this->SendPacket(packet)))
            {
                LOG_LINE_GLOBAL("remote", "ERROR: Unable to send packet. errno:", errno);
                std::cout << "ERROR : SendPacket failed.\n";
            }
            else
            {
                //DisplayPacket("Sended Packet", packet);
            }

            return res;
        }


        SocketResult SendPacketDelete(const char *source)
        {
            SocketResult res = SocketResult::SR_EMPTY;
            MemStreamPacket packet;
            packet.CreatePacketDelete("TEST-MyClient", source);

            if (SocketResult::SR_SUCCESS != (res = this->SendPacket(packet)))
            {
                LOG_LINE_GLOBAL("remote", "ERROR: Unable to send packet. errno:", errno);
                std::cout << "ERROR : SendPacket failed.\n";
            }
            else
            {
                //DisplayPacket("Sended Packet", packet);
            }

            return res;
        }


        SocketResult SendPacketLength(const char *source)
        {
            SocketResult res = SocketResult::SR_EMPTY;
            MemStreamPacket packet;
            packet.CreatePacketLength("TEST-MyClient", source);

            if (SocketResult::SR_SUCCESS != (res = this->SendPacket(packet)))
            {
                LOG_LINE_GLOBAL("remote", "ERROR: Unable to send packet. errno:", errno);
                std::cout << "ERROR : SendPacket failed.\n";
            }
            else
            {
                //DisplayPacket("Sended Packet", packet);
            }

            return res;
        }

        void DisplayPacket(const char *msg, StreamPacket &packet)
        {
            msize_t         pyLen     = packet.PayloadLen();
            msize_t         pyLenRead = 0;
            const msize_t   buffLen   = 32;
            byte_t          buff [buffLen];

            std::stringstream ss;

            for (msize_t i = 0; i < pyLen; i += buffLen)
            {
                if ((pyLenRead = packet.PayloadPart(buff, buffLen, i)) > 0)
                    ss << std::string((char*)buff, pyLenRead);
            }

            std::cout << "ORC Remote Client  - " << msg << " : \n" << ss.str() << std::endl;
        }

};



// *************************************************************************
// ***    Console Implementation                                         ***
// *************************************************************************



// *************************************************************************
// ***                                                                   ***
// ***                   APP                                             ***
// ***                                                                   ***
// ***                                                                   ***
// *************************************************************************



void ReadSync(const char *ip, uint16_t port)
{
    msleep(1);

    SocketClientPacketSyncResponse<SocketTCP> clnt;
    clnt.Address(ip, port);
    if (SocketResult::SR_SUCCESS != clnt.Init())
    {
        std::cerr << "Init ERROR\n";
        return;
    }
    if (SocketResult::SR_SUCCESS != clnt.Connect())
    {
        std::cerr << "Connect ERROR  :  errno:" << ErrnoText(errno) << "-" << errno << std::endl;
        return;
    }
    std::thread th ([&clnt]()->void{clnt.LoopStart();});


    MemStreamPacket packetSend;
    MemStreamPacket packetRecv;
    std::string refid = packetSend.CreatePacketRead("TEST-MyClient", "TableXXX", 10, 0);
    SocketResult res = clnt.SendPacket(/*refid,*/ packetSend, packetRecv);

    std::cout << packetRecv.DumpPayload(std::string("Waited result = ") + SocketResultText(res)) << std::endl;

    std::list<MemStreamPacket::Cmd> cmds;
    MemStreamPacket::DecodePacket(packetRecv, cmds);
    for (const auto &cmd :  cmds)
    {
        std::cout << "Returned cmd ->" << cmd.Dump() << std::endl;
    }

    clnt.LoopStop();
    if (th.joinable()) th.join();
}




void StoreFile(const char *fname, const char *ip, uint16_t port)
{
    SocketClientPacketSyncResponse<SocketTCP> client;
    client.Address(ip, port);
    if (SocketResult::SR_SUCCESS != client.Init())
    {
        std::cerr << "Init ERROR\n";
        return;
    }
    if (SocketResult::SR_SUCCESS != client.Connect())
    {
        std::cerr << "Connect ERROR  :  errno:" << ErrnoText(errno) << "-" << errno << std::endl;
        return;
    }
    std::thread th ([&client]()->void{client.LoopStart();});


    std::ifstream readFile(fname, std::ios::binary);
    if (false == readFile.is_open())
    {
        std::cerr << "ERROR: Unable to open file. (" << fname << ")  - errno : " << ErrnoText(errno) << std::endl;
        return;
    }


    {
        MemStreamPacket packetRecv;

        MemStream<StreamPacket::byte_t> buffWrite;
        buffWrite.Write("0y2x4y6789ABCDEFGHIJ", 20);

        SocketResult res = client.SendPacketWrite(buffWrite, packetRecv, "TEST-SyncFileStore20", "XXX20");
        std::cout << "SendPacketWrite - res:" << SocketResultText(res) << std::endl;

        MemStream<StreamPacket::byte_t> buffRead;
        res = client.SendPacketRead(buffRead, 20, 0, packetRecv, "TEST-SyncFileStore20", "XXX20");
        std::cout << "SendPacketRead - res:" << SocketResultText(res) << std::endl;
        std::cout <<  buffRead.Dump("buff") << std::endl;


        client.LoopStop();
        if (th.joinable()) th.join();
        return;

    }



/*
    std::streampos fsize = readFile.tellg();
    readFile.seekg(0, std::ios::end);
    fsize -=  readFile.tellg();
    readFile.seekg(0, std::ios::beg);

    StreamPacket::byte_t buff[(std::uint64_t)fsize] = "";
    if (readFile.read((char*)buff, fsize))
    {
        MemStreamPacket packetRecv;
        SocketResult res = client.SendPacket(buff, fsize, packetRecv, "TEST-SyncFileStore", fname);
    }
*/



/*
    const int length = _8K;
    char buff[length] = "";

    enum class IS_DONE {no, error, yes } isDone(IS_DONE::no);


    while ((isDone == IS_DONE::no) && (false == readFile.eof()))
    {
        readFile.read(buff, length);
        int crd = readFile.gcount();

        SocketResult res = SocketResult::SR_EMPTY;
        MemStreamPacket packetRecv;
        MemStreamPacket packetSend;
        packetSend.CreatePacketWrite("TEST-SyncFileStore", fname, crd, buff);

        std::cout << "packetSend : \n" << packetSend.DumpPayload() << std::endl;

        if (SocketResult::SR_SUCCESS != (res = client.SendPacket(packetSend, packetRecv)))
        {
            LOG_LINE_GLOBAL("remote", "ERROR: Unable to send packet. errno:", errno);
            std::cout << "ERROR : SendPacket failed.\n";
            isDone = IS_DONE::error;
        }
        else
        {
            std::cout << "packetRecv : \n" << packetRecv.DumpPayload() << std::endl;
        }
    }
*/
    std::cout << "done\n";

    client.LoopStop();
    if (th.joinable()) th.join();
}





int main(int argc, char** argv)
{
    LogLineGlbSocketName(SOCK_PATH_DEFAULT());

    const char *remoteIp    = (argc > 1) ? argv[1] : DEFAULT_REMOTE_IP;
    uint16_t    remotePort  = (argc > 2) ? std::atoi(argv[2]) : DEFAULT_REMOTE_PORT;

    MyClient client ("TestClient");

    std::cout << "\nRemote ORC Test Client\n";
    std::cout << remoteIp << ":" << remotePort << std::endl << std::endl;
    client.Address(remoteIp, remotePort);



    //RemoteClientConsole<MyClient> con(client);

    if (SocketResult::SR_SUCCESS != client.Init())
    {
        std::cerr << "Init ERROR\n";
        return -1;
    }
    if (SocketResult::SR_SUCCESS != client.Connect())
    {
        std::cerr << "Connect ERROR  :  errno:" << ErrnoText(errno) << "-" << errno << std::endl;
        return -2;
    }

    std::thread th ([&client]()->void{client.LoopStart();});


//    client.SendPacketWrite("TableXXX", "");
    client.SendPacketWrite("TableXXX", "1234567890");

//    ReadSync(remoteIp, remotePort);
//    client.SendPacketRead("TableXXX", 10, 0);

/*
    client.SendPacketWrite("TableXXX", "ABCDEFGHIYKLMNOPQRSTUVWXYZ");
    client.SendPacketLength("TableXXX");

    client.SendPacketRead("TableXXX", 36, 0);
    client.SendPacketRead("TableYYY", 36, 0);
*/
/*
    client.SendPacketWrite("TableYYY", "ABCDEFGHIYKLMNOPQRSTUVWXYZ");
    client.SendPacketWrite("TableXXX", "ABCDEFGHIYKLMNOPQRSTUVWXYZabcdefghiyklmnopqrstuvwxyz");
    client.SendPacketWrite("TableXXX", "1234567890ABCDEFGHIYKLMNOPQRSTUVWXYZabcdefghiyklmnopqrstuvwxyz09876543211234567890ABCDEFGHIYKLMNOPQRSTUVWXYZabcdefghiyklmnopqrstuvwxyz0987654321");
    client.SendPacketWrite("TableZZZ",
                            "\nEy Türk Gençliği!\n\n"
                            "Birinci vazifen, Türk istiklâlini, Türk Cumhuriyetini, ilelebet, muhafaza ve müdafaa etmektir.\n\n"
                            "Mevcudiyetinin ve istikbalinin yegâne temeli budur. Bu temel, senin, en kıymetli hazinendir. İstikbalde dahi, seni bu hazineden mahrum etmek isteyecek, dahilî ve haricî bedhahların olacaktır. Bir gün, İstiklâl ve Cumhuriyeti müdafaa mecburiyetine düşersen, vazifeye atılmak için, içinde bulunacağın vaziyetin imkân ve şerâitini düşünmeyeceksin! Bu imkân ve şerâit, çok nâmüsait bir mahiyette tezahür edebilir. İstiklâl ve Cumhuriyetine kastedecek düşmanlar, bütün dünyada emsali görülmemiş bir galibiyetin mümessili olabilirler. Cebren ve hile ile aziz vatanın, bütün kaleleri zaptedilmiş, bütün tersanelerine girilmiş, bütün orduları dağıtılmış ve memleketin her köşesi bilfiil işgal edilmiş olabilir. Bütün bu şerâitten daha elîm ve daha vahim olmak üzere, memleketin dahilinde, iktidara sahip olanlar gaflet ve dalâlet ve hattâ hıyanet içinde bulunabilirler. Hattâ bu iktidar sahipleri şahsî menfaatlerini, müstevlilerin siyasi emelleriyle tevhit edebilirler. Millet, fakr ü zaruret içinde harap ve bîtap düşmüş olabilir.\n\n"
                            "Ey Türk istikbalinin evlâdı! İşte, bu ahval ve şerâit içinde dahi, vazifen; Türk İstiklâl ve Cumhuriyetini kurtarmaktır! Muhtaç olduğun kudret, damarlarındaki asil kanda mevcuttur!\n\n"
                            "Mustafa Kemal Atatürk\n\n"
                            "20 Ekim 1927"   );
    client.SendPacketRead("TableXXX", 10, 30);
    client.SendPacketRead("TableYYY", 10, 5);
    client.SendPacketDelete("TableXXX");
    client.SendPacketDelete("TableYYY");
    client.SendPacketDelete("TableZZZ");
    client.SendPacketDelete("TableTTT");
*/


/*
    //StoreFile("Capture9.PNG");
    StoreFile("Capture9.PNG", "192.168.56.103", 5001);
*/




    msleep(15/*200*/);
    client.LoopStop();
    if (th.joinable()) th.join();

    std::cout << "\nRemote ORC Test Client finished\n";
    return 0;
}

