/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "main.h"


#include <mutex>
#include <condition_variable>
#include <list>
#include <stdio.h>

#include <unistd.h>


TEST(OrcRemoteServerSync, Simple)
{
    ClientServerFrame<SocketTCP, OrcRemoteServerClient, MyTestClientSync>( [](auto &client) -> void
    {
        const char *data = "12345";

        std::list<MemStreamPacket::Cmd> cmdList;

        std::string refidWrite = client.SendPacketWrite("Source-1", data, cmdList);
        EXPECT_FALSE(refidWrite.empty());
        EXPECT_EQ   (1, cmdList.size());
        if(cmdList.size() == 1 )
        {
            EXPECT_EQ   (cmdList.front()._result, std::string("SUCCESS") );
            EXPECT_EQ   (std::strtol(cmdList.front()._buffer._length.c_str(), nullptr, 0), cmdList.front()._buffer._buf.size());
            EXPECT_EQ   (std::strtol(cmdList.front()._buffer._length.c_str(), nullptr, 10), 0);
        }
        cmdList.clear();


        std::string refidRead = client.SendPacketRead ("Source-1", std::strlen(data), 0, cmdList);
        EXPECT_FALSE(refidRead.empty());
        EXPECT_EQ   (1, cmdList.size());
        if(cmdList.size() == 1 )
        {
            MemStream<StreamPacket::byte_t> stream(data, std::strlen(data), "Simple");
            EXPECT_EQ(stream, cmdList.front().DecodeBuffer());
            EXPECT_EQ(std::strtol(cmdList.front()._buffer._length.c_str(), nullptr, 10), cmdList.front()._buffer._buf.size());
        }
    });
}


TEST(OrcRemoteServerSync, ReadNoneExist)
{
    ClientServerFrame<SocketTCP, OrcRemoteServerClient, MyTestClientSync>( [](auto &client) -> void
    {
        std::list<MemStreamPacket::Cmd> cmdList;
        std::string refidRead = client.SendPacketRead ("Source-XXX", 10, 0, cmdList);
        EXPECT_FALSE(refidRead.empty());
        EXPECT_EQ   (1, cmdList.size());

        if (cmdList.size() == 1)
        {
            EXPECT_EQ(cmdList.front()._refid,  refidRead);
            EXPECT_EQ(cmdList.front()._result, "ERROR");
            EXPECT_EQ(std::strtol(cmdList.front()._buffer._length.c_str(), nullptr, 10), cmdList.front()._buffer._buf.size());
        }
    });
}


TEST(OrcRemoteServerSync, Delete)
{
    ClientServerFrame<SocketTCP, OrcRemoteServerClient, MyTestClientSync>( [](auto &client) -> void
    {
        const char *data = "Medyasoft";
        MemStream<StreamPacket::byte_t> stream("Delete");
        stream.Write(data, std::strlen(data));


        std::list<MemStreamPacket::Cmd> cmdList;
        std::string refidWrite = client.SendPacketWrite("Source-1", data, cmdList);
        EXPECT_FALSE(refidWrite.empty());
        EXPECT_EQ   (1, cmdList.size());
        if (cmdList.size() == 1)
        {
            //std::cout << cmdList.front().Dump() << std::endl;

            EXPECT_EQ(cmdList.front()._refid,  refidWrite);
            EXPECT_EQ(cmdList.front()._result, "SUCCESS");
            EXPECT_EQ(std::strtol(cmdList.front()._buffer._length.c_str(), nullptr, 10), cmdList.front()._buffer._buf.size());
            EXPECT_EQ(std::strtol(cmdList.front()._buffer._length.c_str(), nullptr, 10), 0);
        }
        cmdList.clear();


        std::string refidRead = client.SendPacketRead("Source-1", std::strlen(data), 0, cmdList);
        EXPECT_FALSE(refidRead.empty());
        EXPECT_EQ   (1, cmdList.size());
        if (cmdList.size() == 1)
        {
            //std::cout << cmdList.front().Dump() << std::endl;

            EXPECT_EQ(cmdList.front()._refid,  refidRead);
            EXPECT_EQ(cmdList.front()._result, "SUCCESS");
            EXPECT_EQ(std::strtol(cmdList.front()._buffer._length.c_str(), nullptr, 10), cmdList.front()._buffer._buf.size());
            EXPECT_EQ(cmdList.front().DecodeBuffer(), stream);
        }
        cmdList.clear();


        std::string refidDelete = client.SendPacketDelete("Source-1", cmdList);
        EXPECT_FALSE(refidDelete.empty());
        EXPECT_EQ   (1, cmdList.size());
        if (cmdList.size() == 1)
        {
            //std::cout << cmdList.front().Dump() << std::endl;

            EXPECT_EQ(cmdList.front()._refid,  refidDelete);
            EXPECT_EQ(cmdList.front()._result, "SUCCESS");
            EXPECT_EQ(std::strtol(cmdList.front()._buffer._length.c_str(), nullptr, 10), cmdList.front()._buffer._buf.size());
            EXPECT_EQ(std::strtol(cmdList.front()._buffer._length.c_str(), nullptr, 10), 0);
        }
        cmdList.clear();


        std::string refidRead2 = client.SendPacketRead("Source-1", std::strlen(data), 0, cmdList);
        EXPECT_FALSE(refidRead2.empty());
        EXPECT_EQ   (1, cmdList.size());
        if (cmdList.size() == 1)
        {
            //std::cout << cmdList.front().Dump() << std::endl;

            EXPECT_EQ(cmdList.front()._refid,  refidRead2);
            EXPECT_EQ(cmdList.front()._result, "ERROR");
            EXPECT_EQ(std::strtol(cmdList.front()._buffer._length.c_str(), nullptr, 10), cmdList.front()._buffer._buf.size());
            //EXPECT_NE(cmdList.front().DecodeBuffer(), stream);
            EXPECT_EQ(std::strtol(cmdList.front()._buffer._length.c_str(), nullptr, 10), 0);
        }
        cmdList.clear();
    });
}


TEST(OrcRemoteServerSync, DeleteNoneExist)
{
    ClientServerFrame<SocketTCP, OrcRemoteServerClient, MyTestClientSync>( [](auto &client) -> void
    {
        std::list<MemStreamPacket::Cmd> cmdList;

        std::string refidDelete = client.SendPacketDelete("Source-XXX", cmdList);
        EXPECT_FALSE(refidDelete.empty());
        EXPECT_EQ   (1, cmdList.size());
        if (cmdList.size() == 1)
        {
            //std::cout << cmdList.front().Dump() << std::endl;

            EXPECT_EQ(cmdList.front()._refid,  refidDelete);
            EXPECT_EQ(cmdList.front()._result, "ERROR");
            EXPECT_EQ(std::strtol(cmdList.front()._buffer._length.c_str(), nullptr, 10), cmdList.front()._buffer._buf.size());
            EXPECT_EQ(std::strtol(cmdList.front()._buffer._length.c_str(), nullptr, 10), 0);
        }
        cmdList.clear();
    });
}


TEST(OrcRemoteServerSync, SimplePartialSend)
{
    ClientServerFrame<SocketTCP, OrcRemoteServerClient, MyTestClientSync>( [](auto &client) -> void
    {
        const char* dataArr[]
        {
            "12345",
            "67890",
            "abcdefghi",
            "jkl",
            "mnopqrstuvwxyz",
            "ABC",
            "DEFGHIJKLMNO",
            "PQRSTUVWXY",
            "Z",
            "09",
            "87654321"
        };

        std::list<MemStreamPacket::Cmd> cmdList;


        for(const auto data : dataArr)
        {
            std::string refidWrite = client.SendPacketWrite("Source-7", data, cmdList);
            EXPECT_FALSE(refidWrite.empty());
            EXPECT_EQ   (1, cmdList.size());
            if (cmdList.size() == 1)
            {
                //std::cout << cmdList.front().Dump() << std::endl;
                EXPECT_EQ(cmdList.front()._refid,  refidWrite);
                EXPECT_EQ(cmdList.front()._result, "SUCCESS");
                EXPECT_EQ(std::strtol(cmdList.front()._buffer._length.c_str(), nullptr, 10), cmdList.front()._buffer._buf.size());
                EXPECT_EQ(std::strtol(cmdList.front()._buffer._length.c_str(), nullptr, 10), 0);
            }
            cmdList.clear();
        }



        size_t lenExpected = 0;
        MemStream<StreamPacket::byte_t> streamExpected("SimplePartialSend");
        for (const auto data : dataArr)
        {
            size_t len = std::strlen(data);
            lenExpected += len;
            streamExpected.Write(data, len);
        }

        std::string refidRead = client.SendPacketRead ("Source-7", lenExpected, 0, cmdList);
        EXPECT_FALSE(refidRead.empty());
        EXPECT_EQ   (1, cmdList.size());
        if(cmdList.size() == 1 )
        {
            EXPECT_EQ(streamExpected, cmdList.front().DecodeBuffer());
            EXPECT_EQ(std::strtol(cmdList.front()._buffer._length.c_str(), nullptr, 10), cmdList.front()._buffer._buf.size());
        }

        //std::cout << cmdList.front().Dump() << std::endl;

    });
}


TEST(OrcRemoteServerSync, SimplePartialRead)
{
    ClientServerFrame<SocketTCP, OrcRemoteServerClient, MyTestClientSync>( [](auto &client) -> void
    {
        const char* data =  "Ey Türk Gençliği!\n\n"
                            "Birinci vazifen, Türk istiklâlini, Türk Cumhuriyetini, ilelebet, muhafaza ve müdafaa etmektir.\n\n"
                            "Mevcudiyetinin ve istikbalinin yegâne temeli budur. Bu temel, senin, en kıymetli hazinendir. İstikbalde dahi, seni bu hazineden mahrum etmek isteyecek, dahilî ve haricî bedhahların olacaktır. Bir gün, İstiklâl ve Cumhuriyeti müdafaa mecburiyetine düşersen, vazifeye atılmak için, içinde bulunacağın vaziyetin imkân ve şerâitini düşünmeyeceksin! Bu imkân ve şerâit, çok nâmüsait bir mahiyette tezahür edebilir. İstiklâl ve Cumhuriyetine kastedecek düşmanlar, bütün dünyada emsali görülmemiş bir galibiyetin mümessili olabilirler. Cebren ve hile ile aziz vatanın, bütün kaleleri zaptedilmiş, bütün tersanelerine girilmiş, bütün orduları dağıtılmış ve memleketin her köşesi bilfiil işgal edilmiş olabilir. Bütün bu şerâitten daha elîm ve daha vahim olmak üzere, memleketin dahilinde, iktidara sahip olanlar gaflet ve dalâlet ve hattâ hıyanet içinde bulunabilirler. Hattâ bu iktidar sahipleri şahsî menfaatlerini, müstevlilerin siyasi emelleriyle tevhit edebilirler. Millet, fakr ü zaruret içinde harap ve bîtap düşmüş olabilir.\n\n"
                            "Ey Türk istikbalinin evlâdı! İşte, bu ahval ve şerâit içinde dahi, vazifen; Türk İstiklâl ve Cumhuriyetini kurtarmaktır! Muhtaç olduğun kudret, damarlarındaki asil kanda mevcuttur!\n\n"
                            "Mustafa Kemal Atatürk\n"
                            "20 Ekim 1927\n\n";

        const size_t lenPartWrite = 15;
        const size_t lenPartRead  = 19;
        const size_t len          = std::strlen(data);

        std::list<MemStreamPacket::Cmd> cmdList;

        for(size_t i = 0; i < len; i += lenPartWrite)
        {
            std::string refidWrite = client.SendPacketWrite("Source-7", data + i, std::min(lenPartWrite, (len - i)), cmdList);
            EXPECT_FALSE(refidWrite.empty());
            EXPECT_EQ   (1, cmdList.size());
            if (cmdList.size() == 1)
            {
                //std::cout << cmdList.front().Dump() << std::endl;
                EXPECT_EQ(cmdList.front()._refid,  refidWrite);
                EXPECT_EQ(cmdList.front()._result, "SUCCESS");
                EXPECT_EQ(std::strtol(cmdList.front()._buffer._length.c_str(), nullptr, 10), cmdList.front()._buffer._buf.size());
                EXPECT_EQ(std::strtol(cmdList.front()._buffer._length.c_str(), nullptr, 10), 0);
            }
            cmdList.clear();
        }

        for(size_t i = 0, count = 0; i < len; i += lenPartRead, ++count)
        {
            std::string refidRead = client.SendPacketRead ("Source-7", lenPartRead, i, cmdList);

            EXPECT_FALSE(refidRead.empty());
            EXPECT_EQ   (count+1, cmdList.size());
            if (cmdList.size() == count + 1)
            {
                MemStream<StreamPacket::byte_t> bufferRecieved = cmdList.back().DecodeBuffer();
                MemStream<StreamPacket::byte_t> bufferExpected("SimplePartialRead.bufferExpected");
                bufferExpected.Write(data + i, std::min(lenPartRead, (len - i)));

                EXPECT_EQ(cmdList.back()._refid, refidRead);
                EXPECT_EQ(bufferExpected, bufferRecieved);
            }
        }
    });
}


TEST(OrcRemoteServerSync, Length)
{
    ClientServerFrame<SocketTCP, OrcRemoteServerClient, MyTestClientSync>( [](auto &client) -> void
    {
        std::list<MemStreamPacket::Cmd> cmdList;
        std::string refidWrite = client.SendPacketWrite("Source-Length", "1234567890", cmdList);
        EXPECT_FALSE(refidWrite.empty());
        EXPECT_EQ   (1, cmdList.size());
        if (cmdList.size() == 1)
        {
            EXPECT_EQ(cmdList.front()._refid,  refidWrite);
            EXPECT_EQ(cmdList.front()._result, "SUCCESS");
            EXPECT_EQ(std::strtol(cmdList.front()._buffer._length.c_str(), nullptr, 10), cmdList.front()._buffer._buf.size());
            EXPECT_EQ(std::strtol(cmdList.front()._buffer._length.c_str(), nullptr, 10), 0);
        }
        cmdList.clear();

        std::string refidLength = client.SendPacketLength("Source-Length", cmdList);
        EXPECT_FALSE(refidLength.empty());
        EXPECT_EQ   (1, cmdList.size());
        if (cmdList.size() == 1)
        {
            EXPECT_EQ(cmdList.front()._refid, refidLength);
            EXPECT_EQ(cmdList.front()._result, std::string("SUCCESS") );
            EXPECT_EQ(cmdList.front()._buffer._length, std::string("10") );
        }
    });
}


TEST(OrcRemoteServerSync, LengthNoneExist)
{
    ClientServerFrame<SocketTCP, OrcRemoteServerClient, MyTestClientSync>( [](auto &client) -> void
    {
        std::list<MemStreamPacket::Cmd> cmdList;
        std::string refidDelete = client.SendPacketLength("Source-XXX", cmdList);
        EXPECT_FALSE(refidDelete.empty());
        EXPECT_EQ   (1, cmdList.size());
        if (cmdList.size() == 1)
        {
            EXPECT_EQ(cmdList.front()._refid, refidDelete);
            EXPECT_EQ(cmdList.front()._result, std::string("ERROR") );
        }
    });
}


size_t FileContentLength(auto &client, const char *fname)
{
    size_t len = (size_t)-1;

    std::list<MemStreamPacket::Cmd> cmdList;
    std::string refid = client.SendPacketLength(fname, cmdList);

    EXPECT_FALSE(refid.empty());
    EXPECT_EQ   (1, cmdList.size());
    if (cmdList.size() == 1)
    {
        EXPECT_EQ(cmdList.front()._refid, refid);
        EXPECT_EQ(cmdList.front()._result, std::string("SUCCESS") );
        len = std::strtol(cmdList.front()._buffer._length.c_str(), nullptr, 10);
    }

    EXPECT_NE((size_t)-1, len);
    return len;
}


void FileContentSendPartial(auto &client, const char *source, const char *fname)
{
    const size_t blockSizeSend  = _1K;
    size_t       blockCountSend = 0;

    StreamPacket::byte_t blockSend[blockSizeSend];

    FILE *fp = fopen(fname, "rb");
    EXPECT_FALSE(nullptr == fp);
    if (nullptr == fp)
    {
        char xwd[256];
        getcwd(xwd, 256);

        std::cout << "ERROR ---> Unable to open file\n";
        std::cout << "fname  : " << fname << std::endl;
        std::cout << "getcwd : " << xwd   << std::endl;
    }

    if (fp)
    {
        for (int idx = 0; !feof(fp); idx++)
        {
            size_t r = fread (blockSend, 1, blockSizeSend, fp);

            std::list<MemStreamPacket::Cmd> cmdList;
            std::string refid = client.SendPacketWrite(source, blockSend, r, cmdList);
            EXPECT_FALSE(refid.empty());
            EXPECT_EQ   (1, cmdList.size());
            if (cmdList.size() == 1)
            {
                //std::cout << cmdList.front().Dump() << std::endl;
                EXPECT_EQ(cmdList.front()._refid,  refid);
                EXPECT_EQ(cmdList.front()._result, "SUCCESS");
                EXPECT_EQ(std::strtol(cmdList.front()._buffer._length.c_str(), nullptr, 10), cmdList.front()._buffer._buf.size());
                EXPECT_EQ(std::strtol(cmdList.front()._buffer._length.c_str(), nullptr, 10), 0);
            }
        }

        fclose(fp);
    }
}


void FileContentReadPartial(auto &client, const char *source, const char *fname)
{
    const size_t blockSizeRead  = _1K;
    size_t len                  = FileContentLength(client, source);

    StreamPacket::byte_t blockRead[blockSizeRead];

    FILE *fp = fopen(fname, "wb");
    EXPECT_NE(nullptr, fp);
    if (nullptr == fp) return;

    size_t          offset = 0;
    std::uint64_t   lenRecieved = -1;
    std::vector<std::string> refIDsRead;
    for (; /*offset < len*/0 != lenRecieved; offset += blockSizeRead)
    {
        std::list<MemStreamPacket::Cmd> cmdList;
        std::string refid = client.SendPacketRead(source, blockSizeRead, offset, cmdList);
        EXPECT_FALSE(refid.empty());
        EXPECT_EQ   (1, cmdList.size());
        if (cmdList.size() == 1)
        {
            EXPECT_EQ(cmdList.back()._refid,    refid);
            EXPECT_EQ(cmdList.back()._source,   source);
            EXPECT_EQ(cmdList.back()._cmdid,    MemStreamPacket::CmdID::CMD_READ);
            EXPECT_EQ(cmdList.back()._result,   std::string("SUCCESS") );
            EXPECT_EQ(std::strtol(cmdList.back()._buffer._length.c_str(), nullptr, 10), cmdList.back()._buffer._buf.size());
            lenRecieved = std::strtol(cmdList.back()._buffer._length.c_str(), nullptr, 10);


            MemStream<StreamPacket::byte_t> bufferRecieved = cmdList.back().DecodeBuffer();
            std::uint64_t l = bufferRecieved.Len();
            char buff[l];
            std::uint64_t rl = bufferRecieved.Read(buff, l, 0);
            fwrite(buff, 1, rl, fp);
        }

    }

    fclose(fp);
}



TEST(OrcRemoteServerSync, SimplePartialLargeData)
{
    ClientServerFrame<SocketTCP, OrcRemoteServerClient, MyTestClientSync>( [](auto &client) -> void
    {
        const char *fname     = /*"./OrcRemoteServerTCP/gtest/*/"ImageSource.PNG";
        const char *fnameNew  = /*"./OrcRemoteServerTCP/gtest/*/"ImageSourceNewSync.PNG";
        const char *source    = fname;

        std::remove(fnameNew);

        FileContentSendPartial(client, source, fname);
        FileContentReadPartial(client, source, fnameNew);

        EXPECT_TRUE(CompareFile(fname, fnameNew));

        std::remove(fnameNew);
    });
}



TEST(OrcRemoteServerSync, SimpleBulk1)
{
    auto functor = [](auto &client) -> void
    {
        MemStreamPacket                 packetRecv1;
        MemStreamPacket                 packetRecv2;
        std::list<MemStreamPacket::Cmd> cmds1;
        std::list<MemStreamPacket::Cmd> cmds2;

        MemStream<StreamPacket::byte_t> buffWrite;
        MemStream<StreamPacket::byte_t> buffRead;


        const char *data = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ9876543210abcdefghijklmnopqrstuvwxyz";
        int len = std::strlen(data);
        //buffWrite.Write(data, len);

        const char *source      = "SimpleBulk1-Client-Table";
        const char *nameClient  = "TEST-SimpleBlock-Client";


        int lenPart = 7;
        int ofs = 0;
        for (; ofs < len; ofs+=lenPart)
        {
            buffWrite.reset();
            buffWrite.Write(data + ofs, lenPart);
            EXPECT_EQ(SocketResult::SR_SUCCESS, client.SendPacketWrite(buffWrite, packetRecv1, nameClient, source));
//        std::cout << "SEND packetRecv:\n" << packetRecv1.DumpPayload("") << std::endl;
            packetRecv1.Reset();
            MemStreamPacket::DecodePacket(packetRecv1, cmds1);
            for (const auto &cmd : cmds1)
            {
                EXPECT_EQ(cmd._source, source);
                EXPECT_EQ(cmd._cmdid , MemStreamPacket::CmdID::CMD_WRITE);
                EXPECT_EQ(cmd._result, std::string("SUCCESS"));
            }
        }

        if (len > ofs)
        {
            buffWrite.reset();
            buffWrite.Write(data + ofs, len - ofs);
            EXPECT_EQ(SocketResult::SR_SUCCESS, client.SendPacketWrite(buffWrite, packetRecv1, nameClient, source));
//            std::cout << "SEND packetRecv:\n" << packetRecv1.DumpPayload("") << std::endl;
            packetRecv1.Reset();
            MemStreamPacket::DecodePacket(packetRecv1, cmds1);
            for (const auto &cmd : cmds1)
            {
                EXPECT_EQ(cmd._source, source);
                EXPECT_EQ(cmd._cmdid , MemStreamPacket::CmdID::CMD_WRITE);
                EXPECT_EQ(cmd._result, std::string("SUCCESS"));
            }
        }



        
        EXPECT_EQ(SocketResult::SR_SUCCESS, client.SendPacketRead(buffRead, len, 0, packetRecv2, nameClient, source));
//        std::cout << "..............RECV packetRecv:\n" << packetRecv2.DumpPayload("") << std::endl;
//        std::cout << "..............RECV buffRead1 :\n" << buffRead.Dump("") << std::endl;
        MemStreamPacket::DecodePacket(packetRecv2, cmds2);
        for (const auto &cmd : cmds2)
        {
            EXPECT_EQ(cmd._source, source);
            EXPECT_EQ(cmd._cmdid , MemStreamPacket::CmdID::CMD_READ);
            EXPECT_EQ(cmd._result, std::string("SUCCESS"));
        }
        EXPECT_EQ(MemStream<StreamPacket::byte_t>(data, len), buffRead);
    };

    ClientServerFrame<SocketTCP, OrcRemoteServerClient, SocketClientPacketSyncResponse<SocketTCP>>(functor);
}








TEST(OrcRemoteServerSync, SimpleBulk2)
{
    auto functor = [](auto &client) -> void
    {
        MemStreamPacket                 packetRecv1;
        MemStreamPacket                 packetRecv2;
        std::list<MemStreamPacket::Cmd> cmds1;
        std::list<MemStreamPacket::Cmd> cmds2;

        MemStream<StreamPacket::byte_t> buffWrite;
        MemStream<StreamPacket::byte_t> buffRead;

        const char *data = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ9876543210abcdefghijklmnopqrstuvwxyz";
        int len = std::strlen(data);
        buffWrite.Write(data, len);

        const char *source      = "SimpleBulk2-Client-Table";
        const char *nameClient  = "TEST-SimpleBlock-Client";


        EXPECT_EQ(SocketResult::SR_SUCCESS, client.SendPacketWrite(buffWrite, packetRecv1, nameClient, source));
//        std::cout << "SEND packetRecv:\n" << packetRecv1.DumpPayload("") << std::endl;
        MemStreamPacket::DecodePacket(packetRecv1, cmds1);
        for (const auto &cmd : cmds1)
        {
            EXPECT_EQ(cmd._source, source);
            EXPECT_EQ(cmd._cmdid , MemStreamPacket::CmdID::CMD_WRITE);
            EXPECT_EQ(cmd._result, std::string("SUCCESS"));
        }


        EXPECT_EQ(SocketResult::SR_SUCCESS, client.SendPacketRead(buffRead, 10, 36, packetRecv2, nameClient, source));
//        std::cout << "..............RECV packetRecv:\n" << packetRecv2.DumpPayload("") << std::endl;
//        std::cout << "..............RECV buffRead2 :\n" << buffRead2.Dump("") << std::endl;
        MemStreamPacket::DecodePacket(packetRecv2, cmds2);
        for (const auto &cmd : cmds2)
        {
            EXPECT_EQ(cmd._source, source);
            EXPECT_EQ(cmd._cmdid , MemStreamPacket::CmdID::CMD_READ);
            EXPECT_EQ(cmd._result, std::string("SUCCESS"));
        }
        EXPECT_EQ(buffRead, MemStream<StreamPacket::byte_t>("9876543210", 10));
    };

    ClientServerFrame<SocketTCP, OrcRemoteServerClient, SocketClientPacketSyncResponse<SocketTCP>>(functor);
}


TEST(OrcRemoteServerSync, SimpleBulk3)
{
    auto functor = [](auto &client) -> void
    {
        MemStreamPacket                 packetRecv1;
        MemStreamPacket                 packetRecv2;
        std::list<MemStreamPacket::Cmd> cmds1;
        std::list<MemStreamPacket::Cmd> cmds2;

        MemStream<StreamPacket::byte_t> buffPartial;
        MemStream<StreamPacket::byte_t> buffWrite;
        MemStream<StreamPacket::byte_t> buffRead;


        const char *data = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ9876543210abcdefghijklmnopqrstuvwxyz";
        int len = std::strlen(data);
        buffWrite.Write(data, len);

        const char *source      = "SimpleBulk3-Client-Table";
        const char *nameClient  = "TEST-SimpleBlock-Client";


        EXPECT_EQ(SocketResult::SR_SUCCESS, client.SendPacketWrite(buffWrite, packetRecv1, nameClient, source));
//        std::cout << "SEND packetRecv:\n" << packetRecv1.DumpPayload("") << std::endl;
        MemStreamPacket::DecodePacket(packetRecv1, cmds1);
        for (const auto &cmd : cmds1)
        {
            EXPECT_EQ(cmd._source, source);
            EXPECT_EQ(cmd._cmdid , MemStreamPacket::CmdID::CMD_WRITE);
            EXPECT_EQ(cmd._result, std::string("SUCCESS"));
        }

        int xxx = 0;

        int lenPart = 5;
        int ofs     = 0;

        for (; ofs < len; ofs += lenPart)
        {
            buffPartial.reset();
            EXPECT_EQ(SocketResult::SR_SUCCESS, client.SendPacketRead(buffPartial, lenPart, ofs, packetRecv2, nameClient, source));
//            std::cout << "buffPartial:\n" << buffPartial.Dump(std::to_string(++xxx)) << std::endl;
            MemStreamPacket::DecodePacket(packetRecv2, cmds2);
            for (const auto &cmd : cmds2)
            {
                EXPECT_EQ(cmd._source, source);
                EXPECT_EQ(cmd._cmdid , MemStreamPacket::CmdID::CMD_READ);
                EXPECT_EQ(cmd._result, std::string("SUCCESS"));
            }
            buffRead += buffPartial;
        }




        // rest
        if (len > ofs)
        {
            buffPartial.reset();
            EXPECT_EQ(SocketResult::SR_SUCCESS, client.SendPacketRead(buffPartial, len - ofs, ofs, packetRecv2, nameClient, source));
//            std::cout << "buffPartial..:\n" << buffPartial.Dump(std::to_string(++xxx)) << std::endl;
            MemStreamPacket::DecodePacket(packetRecv2, cmds2);
            for (const auto &cmd : cmds2)
            {
                EXPECT_EQ(cmd._source, source);
                EXPECT_EQ(cmd._cmdid , MemStreamPacket::CmdID::CMD_READ);
                EXPECT_EQ(cmd._result, std::string("SUCCESS"));
            }
            buffRead += buffPartial;
        }
        EXPECT_EQ(buffWrite, buffRead);
    };

    ClientServerFrame<SocketTCP, OrcRemoteServerClient, SocketClientPacketSyncResponse<SocketTCP>>(functor);
}


MemStream<StreamPacket::byte_t> FileContentLoad(const char *fname)
{
    MemStream<StreamPacket::byte_t> fileContent;

    const size_t buffSize  = _32K;
    StreamPacket::byte_t buff[buffSize];

    FILE *fp = fopen(fname, "rb");
    EXPECT_FALSE(nullptr == fp);
    if (nullptr == fp)
    {
        char xwd[256];
        getcwd(xwd, 256);

        std::cout << std::endl;
        std::cout << "ERROR ---> Unable to open file\n";
        std::cout << "fname  : " << fname << std::endl;
        std::cout << "getcwd : " << xwd   << std::endl;
        std::cout << std::endl;
    }

    if (fp)
    {
        for (int idx = 0; !feof(fp); idx++)
        {
            size_t r = fread (buff, 1, buffSize, fp);
            fileContent.Write(buff, r);
        }
        fclose(fp);
    }

    return std::move(fileContent);
}




TEST(OrcRemoteServerSync, SimpleBulkLargeData)
{
    auto functor = [](auto &client) -> void
    {
        const char *fname     = /*"./OrcRemoteServerTCP/gtest/*/"ImageSource.PNG";
        const char *fnameNew  = /*"./OrcRemoteServerTCP/gtest/*/"ImageSourceNewSync.PNG";
        const char *source    = fname;
        std::int64_t fsize    = FileSize(fname);

        EXPECT_GT(fsize, -1);

        std::remove(fnameNew);

        MemStream<StreamPacket::byte_t> streamRead;
        MemStream<StreamPacket::byte_t> fileContent = FileContentLoad(fname);
        EXPECT_EQ(fsize, fileContent.Len());

        {
            StreamPacket packetRecv;
            EXPECT_EQ(SocketResult::SR_SUCCESS, client.SendPacketWrite(fileContent, packetRecv, "SimpleBulkLargeData", fname));
            std::list<MemStreamPacket::Cmd> cmds;
            MemStreamPacket::DecodePacket(packetRecv, cmds);
            EXPECT_EQ(cmds.size(), 1);
            for (const auto &cmd : cmds)
            {
                EXPECT_EQ(cmd._source, source);
                EXPECT_EQ(cmd._cmdid , MemStreamPacket::CmdID::CMD_WRITE);
                EXPECT_EQ(cmd._result, std::string("SUCCESS"));
            }
        }

        {
            StreamPacket packetRecv2;
            EXPECT_EQ(SocketResult::SR_SUCCESS, client.SendPacketRead(streamRead, fsize, 0, packetRecv2, "SimpleBulkLargeData", fname));
            std::list<MemStreamPacket::Cmd> cmds2;
            MemStreamPacket::DecodePacket(packetRecv2, cmds2);
            for (const auto &cmd : cmds2)
            {
                EXPECT_EQ(cmd._source, source);
                EXPECT_EQ(cmd._cmdid , MemStreamPacket::CmdID::CMD_READ);
                EXPECT_EQ(cmd._result, std::string("SUCCESS"));
            }
        }

        EXPECT_EQ(fileContent, streamRead);
    };
    ClientServerFrame<SocketTCP, OrcRemoteServerClient, SocketClientPacketSyncResponse<SocketTCP>>(functor);
}


