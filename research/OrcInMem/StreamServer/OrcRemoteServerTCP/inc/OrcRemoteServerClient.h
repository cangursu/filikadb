/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   OrcRemoteServerClient.h
 * Author: can.gursu
 *
 * Created on 17 October 2018, 11:07
 */

#ifndef __ORC_REMOTE_SERVER_CLIENT_H__
#define __ORC_REMOTE_SERVER_CLIENT_H__

#include "SocketClientPacket.h"
#include "SocketClientPacketSyncResponse.h"
#include "Console.h"
#include "SocketTCP.h"
//#include "MemStreamMap.h"
#include "MemStreamStore.h"
#include "MemStreamPacket.h"

#include <ctime>


class OrcRemoteServerClient : public SocketClientPacket<SocketTCP>
{
    using  msize_t = StreamPacket::msize_t;
public:
    OrcRemoteServerClient(const char *name = "OrcRemoteServerClient")
            : SocketClientPacket<SocketTCP>(name)
    {
    }

    OrcRemoteServerClient(int fd, const char *name)
        : SocketClientPacket<SocketTCP>(fd, name)
    {

    }

    virtual void OnErrorClient (SocketResult err)
    {
        std::cerr << "Error : OrcRemoteServerClient::OnErrorClient - " << this->Name() << " : " << SocketResultText(err) << std::endl;
    }

    void            DisplayPacket       (StreamPacket &packet, const char *msg = nullptr);

    virtual void    OnRecvPacket        (StreamPacket &&packet)
    {
        if (nullptr == _pstore)
            return;

        std::vector<MemStreamPacket> responses;
        _pstore->Packet(packet, responses, Silent() ? nullptr : this->Name().c_str());

        for(const auto &pck : responses)
        {
            SendPacket(pck);
        }
    }

    void SetStore(MemStreamStore *store)
    {
        _pstore = store;
    }

    void Silent(bool set) { _isSilent = set; }
    bool Silent()         { return _isSilent; }

private:
    bool           _isSilent = false;
    MemStreamStore *_pstore = nullptr;
};





inline void OrcRemoteServerClient::DisplayPacket(StreamPacket &packet, const char *msg /* = nullptr */)
{
    const msize_t        buffLen = 512;
    StreamPacket::byte_t buff [buffLen];

    if (msg)
        std::cout << "Packet : " << msg << std::endl;

    msize_t pyLenPart = 0;
    msize_t pyLen = packet.PayloadLen();
    for (msize_t offset = 0; offset < pyLen; offset += buffLen)
    {
        if ( (pyLenPart = packet.PayloadPart(buff, buffLen, offset)) > 0)
            std::cout << std::string((char*)buff, pyLenPart);
    }
    std::cout << std::endl;
}

#endif /* __ORC_REMOTE_SERVER_CLIENT_H__ */

