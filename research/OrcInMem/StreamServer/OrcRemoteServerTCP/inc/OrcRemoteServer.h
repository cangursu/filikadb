/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   OrcRemoteServer.h
 * Author: can.gursu
 *
 * Created on 17 October 2018, 11:08
 */

#ifndef __ORC_REMOTE_SERVER_H__
#define __ORC_REMOTE_SERVER_H__


#include "SocketServer.h"
#include "StreamPacket.h"
#include "MemStreamStore.h"




template <typename TSockSrv, typename TSockCln>
class OrcRemoteServer : public SocketServer<TSockSrv, TSockCln>
{
    using  msize_t = StreamPacket::msize_t;

    public:

        OrcRemoteServer(const char *name, bool isSilent = false) : SocketServer<TSockSrv, TSockCln>(name), _isSilent(isSilent)
        {
            if (!_isSilent)
                std::cout << "OrcRemoteServer - " << name  << std::endl;
        }


        virtual void OnAccept(TSockCln &sock, const sockaddr &addr)
        {
            sock.SetStore(&_storeInstant);
            sock.Silent(Silent());
            if (!_isSilent)
                std::cout << "Accepted connection - " << PrintClientCount()  << std::endl;
        }
/*
        virtual void OnRecv(TSockCln &sock, MemStream<std::uint8_t> &&stream)
        {
            //if (!_isSilent)
                //std::cout << stream.dump("OrcRemoteServer::OnRecv");
            sock.OnRecv(std::move(stream));
        }
*/
        virtual void OnDisconnect  (const TSockCln &sock)
        {
            if (!_isSilent)
                std::cout << "Client Disconnected - " << PrintClientCount()  << std::endl;
        }

        virtual void OnErrorClient(const TSockCln &cln, SocketResult res)
        {
            if (!_isSilent)
                std::cout << "ErrorClient : " << SocketResultText(res) << std::endl;
        }

        virtual void OnErrorServer(SocketResult res)
        {
           if (!_isSilent)
                std::cout << "ErrorServer : " << SocketResultText(res) << std::endl;
        };

        std::string PrintClientCount()
        {
            std::stringstream ss;
            if (!_isSilent)
                ss << "Client count = " << SocketServer<TSockSrv, TSockCln>::ClientCount();
            return std::move(ss.str());
        }

        void Silent(bool set) { _isSilent = set; }
        bool Silent()         { return _isSilent; }

    private:
        MemStreamStore _storeInstant;
        bool           _isSilent = false;
};





#endif /* __ORC_REMOTE_SERVER_H__ */

