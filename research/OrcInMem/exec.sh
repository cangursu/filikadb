
scriptName=$1
res=0

find . -type f -name $scriptName -print0 | while read -d $'\0' file
do
    dir=$(dirname $file)
    pushd $dir
    ./$scriptName
    res=$?
    if [ "$res" != "0" ]; then
        echo "Execution has been stopped due to a failure (res=$res)."
        exit $res;
    fi
    popd 
done

res=$?
exit $res
