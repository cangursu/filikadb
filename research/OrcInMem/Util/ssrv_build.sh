#!/bin/sh


echo ............................................................................
echo Build Utils
echo ............................................................................

pushd .
set -e

find . -type f -name "Makefile" -print0 | while read -d $'\0' file
do
    dir=$(dirname $file)
    echo
    echo Building  $dir
    echo
    make -C $dir -f ./Makefile
    res=$?
    if [ "$res" != "0" ]; then
        echo ERROR : $dir
        exit $res
    fi
done
res=$?

popd

exit $res

