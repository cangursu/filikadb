#!/bin/sh

echo ............................................................................
echo Unit Test Util
echo ............................................................................


find . -type f -name "gtestapp" -print0 | while read -d $'\0' filepath
do
    dir=$(dirname $filepath)
    fname=$(basename $filepath)

    echo
    echo Testing  $dir
    echo

    pushd $dir
    ./$fname
    res=$?
    popd

    if [ "$res" != "0" ]; then
        echo "ERROR : Test $dir  failed ...."
        echo "Test process interrupted. (res:$res)"
        exit $res;
    fi
done

res=$?

if [ "$res" == "0" ]; then
    echo All tests executed successfully
fi

exit $res
