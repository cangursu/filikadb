#!/bin/sh

echo ............................................................................
echo Build All 
echo ............................................................................

sh build_tools.sh
res=$?

if [ "$res" != "0" ]; then
    echo ERROR ....
    exit $res
fi

sh exec.sh "ssrv_build_pgext.sh"
res=$?


exit $res

