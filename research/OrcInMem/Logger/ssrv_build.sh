#!/bin/sh


echo ............................................................................
echo Build Server, Log, Tools 
echo ............................................................................

set -e

#......pushd .
#......
#......res=0
#......pushd ./3rdParty
#......./buildall.sh
#......res=$?
#......popd



find . -type f -name "Makefile" -print0 | while read -d $'\0' file
do
    dir=$(dirname $file)
    echo
    echo Building  $dir
    echo
    make -C $dir -f ./Makefile
    res=$?
    if [ "$?" != "0" ]; then
        echo ERROR : $dir
        exit $res
    fi
done

res=$?


#pushd ./pgext/
#sudo make install
#make installcheck


#popd
