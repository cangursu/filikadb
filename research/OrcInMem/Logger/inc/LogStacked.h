

#ifndef __LOG_STACKED_H__
#define __LOG_STACKED_H__


class LogStacked
{
public:
    template<typename ... Args>
    LogStacked(const char *desc, const int line /*= __LINE__*/, const char *filenc /*= __FILENC__*/, const char *func /*= __func__*/, Args ... args)
        : _desc(desc)
    {
        std::string s("-> ");
        s += _desc;
        LogLineGlobalFormatPacket(s.c_str(), line, filenc, func, args ...);
        ++_indent;
    }
    ~LogStacked()
    {
        std::string s("-< ");
        s += _desc;
        LogLineGlobalFormatPacket(s.c_str(), 0, "", "");
        --_indent;
    }

    //template<typename ... Args>
    void log(char const *desc, int line /*= __LINE__*/, const char *filenc /*= __FILENC__*/, const char *func /*= __func__*/ )//, Args ... args)
    {
        LogLineGlobalFormatPacket(desc, line, filenc, func);
    }

    static int  _indent;
    std::string _desc;
    std::string _line;
    std::string _filenc;
    std::string _func;
};

#define LOG_STACKED_GLOBAL(lgsck, desc, ...) LogStacked lgsck(desc, __LINE__, __FILENC__, __func__, ## __VA_ARGS__)


#endif //__LOG_STACKED_H__