#!/bin/sh

echo ............................................................................
echo Clean All 
echo ............................................................................


pushd .

find . -name "Makefile" -print0 | while read -d $'\0' file
do
    dir=$(dirname $file)
    make -C $dir -f ./Makefile clean
done

find . -type d -name "_build"    -print0 | xargs -0 rm -fvdr
find . -type f -name "core.*"    -print0 | xargs -0 rm -fvd
find . -type f -name "*.class"   -print0 | xargs -0 rm -fvd
find . -type d -name "nbproject" -print0 | xargs -0 rm -fvdr


popd
