

#ifndef __EXT_ORC15_ORC_STREAM_REMOTE_H__
#define __EXT_ORC15_ORC_STREAM_REMOTE_H__

#include "orc/OrcFile.hh"
#include "MemStream.h"
#include "Logger.h"
#include "LogStacked.h"
#include "SocketTCP.h"
#include "SocketClientPacketSyncResponse.h"
#include "thread"




class OrcStreamRemote   : public orc::OutputStream
                        , public orc::InputStream
                        , public SocketClientPacketSyncResponse<SocketTCP>
{
public:
    OrcStreamRemote(const std::string &name, const std::string &source)
        : SocketClientPacketSyncResponse<SocketTCP>(name.c_str())
        , _name(name)
        , _source(source)
    {
        //LOG_STACKED_GLOBAL(lgs, "", "Construct");
    }
    ~OrcStreamRemote() override
    {
        //LOG_STACKED_GLOBAL(lgs, "", "Destruct");
        close();
        Release();
    }

    SocketResult Init()
    {
        //LOG_STACKED_GLOBAL(lgs, "");

        SocketResult res;
        if (SocketResult::SR_SUCCESS != (res = SocketClientPacketSyncResponse<SocketTCP>::Init()))
        {
            LOG_LINE_GLOBAL("remote", "ERROR: Unable to initialize OrcStreamRemote");
        }
        else if (SocketResult::SR_SUCCESS != (res = this->Connect()))
        {
            LOG_LINE_GLOBAL("remote", "ERROR: Unable to Connect SocketTCP - ", PrmDesc());
            LOG_LINE_GLOBAL("remote", PrmDesc());
        }
        else
        {
            LOG_LINE_GLOBAL("remote", "Conect Adress=", this->Address(), ":", this->Port());
        }

        _th = std::thread{[this]()->void{this->LoopStart();}};
        return res;
    }

    SocketResult Release ()
    {
        //LOG_STACKED_GLOBAL(lgs, "");
        LoopStop();
        if (_th.joinable()) _th.join();
        return SocketClientPacketSyncResponse<SocketTCP>::Release();
    }

    uint64_t getLength() const override
    {
        return _length;
        //std::uint64_t length = 0;
/*
        SocketResult res = SocketResult::SR_EMPTY;

        std::list<MemStreamPacket::Cmd> cmdList;

        MemStreamPacket packet;
        MemStreamPacket packetResult;

        std::string refid = packet.CreatePacketLength(_name.c_str(), _source.c_str());
        if (refid.size() > 0)
            res = this->SendPacket(packet, packetResult);
        if (res != SocketResult::SR_SUCCESS)
            refid.clear();
        else
        {
            MemStreamPacket::DecodePacket(packetResult, cmdList);
            if (cmdList.size() == 1)
                length = std::stol(cmdList.front()._buffer._length);
        }
        return length;
*/
    }
    uint64_t getNaturalWriteSize() const override
    {
        //LOG_STACKED_GLOBAL(lgs, "");
        return _32K;
    }
    virtual uint64_t getNaturalReadSize() const override
    {
        //LOG_STACKED_GLOBAL(lgs, "");
        return _32K;
    }
    virtual void write (const void* buf, size_t length) override
    {
        //LOG_STACKED_GLOBAL(lgs, "", "length:", length);

        MemStream<StreamPacket::byte_t> streamBuffer;
        streamBuffer.Write(buf, length);

        MemStreamPacket packetRecv;
        SocketResult result = SendPacketWrite(streamBuffer, packetRecv, _name.c_str(), _source.c_str());
        //LOG_LINE_GLOBAL("remote", "Write res=", SocketResultText(result));
        if (result != SocketResult::SR_SUCCESS)
        {
            LOG_LINE_GLOBAL(  "remote", "ERROR: Unable to send packet."
                            , " result:", SocketResultText(result)
                            , " errno:", ErrnoText(errno)  );
            LOG_LINE_GLOBAL("remote", "packetRecv:\n", packetRecv.DumpPayload());
            //elog(LOG, "ERROR : SendPacket failed.");
        }
        else
        {
            _length += length;
            //TODO: Check packetRecv for successness
            //LOG_LINE_GLOBAL("remote", "packetRecv = \n", packetRecv.DumpPayload("packetRecv"));
        }
    }

    virtual void read  (void* buf, uint64_t length, uint64_t offset) override
    {
        //LOG_STACKED_GLOBAL(lgs, "", "length:", length, "-offset:", offset);

        MemStream<StreamPacket::byte_t> streamRead;
        MemStreamPacket                 packetRecv;
        SocketResult result = SendPacketRead(streamRead, length, offset, packetRecv, _name.c_str(), _source.c_str());
        if (result == SocketResult::SR_SUCCESS)
        {
            //TODO: Check packetRecv for successness
            streamRead.Read(buf, length);
        }
        else
        {
            LOG_LINE_GLOBAL("remote", "ERROR: Unable to send packet."
                                    , " result : ", SocketResultText(result)
                                    , " errno : ", ErrnoText(errno)  );
            //elog(LOG, "ERROR : SendPacket failed.");
        }
    }
    const std::string& getName() const override
    {
        //LOG_STACKED_GLOBAL(lgs, "", "_name:", _name);
        return _name;
    }
    void close() override
    {
        //LOG_STACKED_GLOBAL(lgs, "");
    }

private :
    std::string         _name;
    std::string         _source;
    std::uint64_t       _length = 0;
    //std::string         _sender;
    std::thread         _th;
};


#endif  //__EXT_ORC15_ORC_STREAM_REMOTE_H__


