#!/bin/sh


make -C ./GenTestData/  -f Makefile clean

until pg_isready -q
do
    echo .... start db ....
    pg_ctl -D ~/pgdata -l ~/pgdata/log start
    sleep 2
done


echo .... drop ext. ....
psql -c "drop extension orcinmem;"


echo .... stop db ....
pg_ctl -D ~/pgdata -l ~/pgdata/log stop


make -f Makefile clean && sudo make -f Makefile uninstall

if [ "$?" != "0" ]; then
    echo
    echo .... build error ....
else
    echo
    echo .... start db ....
    echo
    pg_ctl -D ~/pgdata -l ~/pgdata/log start
    rs=$?
fi


find . -type d -name "_build"    -print0 | xargs -0 rm -fdvr
find . -type f -name "core.*"    -print0 | xargs -0 rm -fdv
find . -type f -name "*.class"   -print0 | xargs -0 rm -fvd
find . -type d -name "nbproject" -print0 | xargs -0 rm -fdvr

exit $rs
