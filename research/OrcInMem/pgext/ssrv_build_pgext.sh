#!/bin/sh

echo ............................................................................
echo Build PG Extension
echo ............................................................................

make -C ./GenTestData/  -f Makefile

until pg_isready -q
do
    echo .... start db ....
    pg_ctl -D ~/pgdata -l ~/pgdata/log start
    sleep 2
done


echo .... drop ext ....
psql -c "drop extension orcinmem;"

echo .... stop db ....
pg_ctl -D ~/pgdata -l ~/pgdata/log stop

echo
echo .... build ....
echo

make -f Makefile all && sudo make -f Makefile install
rs=$?


if [ "$rs" != "0" ]; then
    echo
    echo .... build error ....
else
    echo
    echo .... start db ....
    echo
    
    pg_ctl -D ~/pgdata -l ~/pgdata/log start

    echo
    echo .... create ext ....
    echo
    psql -c "create extension orcinmem;"
    rs=$?
fi

exit $rs