#!/bin/sh

echo ............................................................................
echo Clean All 
echo ............................................................................

./clean_tools.sh
res=$?
if [ "$res" != "0" ]; then
    echo ERROR ....
    exit $res
fi

./exec.sh "ssrv_clean_pgext.sh"
res=$?


exit $res