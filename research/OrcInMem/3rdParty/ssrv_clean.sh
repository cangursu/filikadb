#!/bin/sh

echo ............................................................................
echo Cleaning 3rdParty
echo ............................................................................


find . -type f -name "Makefile" -print0 | while read -d $'\0' file
do
    dir=$(dirname $file)
    echo
    echo Cleaning  $dir
    echo
    make -C $dir -f ./Makefile clean
done

find . -type d -name "_build"    -print0 | xargs -0 rm -fdvr
find . -type f -name "core.*"    -print0 | xargs -0 rm -fdv
find . -type f -name "*.class"   -print0 | xargs -0 rm -fvd
find . -type d -name "nbproject" -print0 | xargs -0 rm -fdvr

