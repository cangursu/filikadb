#!/bin/sh


echo ............................................................................
echo Build All 3rd Party
echo ............................................................................

pushd .
set -e


export AVX2_CFLAGS=-mavx2 
export SSSE3_CFLAGS=-mssse3 
export SSE41_CFLAGS=-msse4.1 
export SSE42_CFLAGS=-msse4.2 
export AVX_CFLAGS=-mavx 
export CFLAGS+=-fPIC


echo
echo Building  ./base64/
echo
make -C ./base64/ -f ./Makefile

echo
echo Building  ./gtest/
echo
pushd ./gtest/
mkdir -p _build && cd _build
cmake ..
make -f ./Makefile
popd

echo
echo Building  ./orc15/
echo
pushd ./orc15/
mkdir -p _build && cd _build
cmake -DCMAKE_CXX_FLAGS="${CMAKE_CXX_FLAGS} -fPIC" -DCMAKE_C_FLAGS="${CMAKE_C_FLAGS} -fPIC"  -DBUILD_JAVA=OFF -DBUILD_LIBHDFSPP=OFF -DCMAKE_BUILD_TYPE=DEBUG ..
make package test-out
popd


#find . -type f -name "Makefile" -print0 | while read -d $'\0' file
#do
#    dir=$(dirname $file)
#
#    echo
#    echo Building  $dir
#    echo
#
#    make -C $dir -f ./Makefile
#    res=$?
#    if [ "$res" != "0" ]; then
#        echo ERROR : $dir
#        exit $res
#    fi
#done


res=$?

popd
exit $res
